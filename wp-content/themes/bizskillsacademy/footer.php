			<!-- footer -->
			<footer class="footer" role="contentinfo">

				<!-- copyright -->
				<p class="copyright">
					&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>. <?php _e('Powered by', 'html5blank'); ?>
					<a href="//wordpress.org" title="WordPress">WordPress</a> &amp; <a href="//html5blank.com" title="HTML5 Blank">HTML5 Blank</a>.
				</p>
				<!-- /copyright -->

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<script type="text/javascript">
		 jQuery(document).ready(function () {
				jQuery(document).on("scroll", onScroll);

				jQuery('a[href^="#"]').on('click', function (e) {
					e.preventDefault();
					jQuery(document).off("scroll");

					jQuery('a').each(function () {
						jQuery(this).removeClass('active');
					})
					jQuery(this).addClass('active');

					var target = this.hash;
					$target = jQuery(target);
					jQuery('html, body').stop().animate({
						'scrollTop': $target.offset().top+2
					}, 500, 'swing', function () {
						window.location.hash = target;
						jQuery(document).on("scroll", onScroll);
					});
				});
				
			});
			

			function onScroll(event){
				var scrollPosition = jQuery(document).scrollTop();
				jQuery('nav a').each(function () {
					var currentLink = jQuery(this);
					var refElement = jQuery(currentLink.attr("href"));
					if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
						jQuery('nav ul li a').removeClass("active");
						currentLink.addClass("active");
						if(currentLink.attr("href") != "#home"){
							jQuery('#header_top').addClass("nav_stick");
						}
					}
					else{
						currentLink.removeClass("active");
						if(currentLink.attr("href") == "#home"){
						jQuery('#header_top').removeClass("nav_stick");
					}	}
				});
			}
			
	</script>

	</body>
</html>
