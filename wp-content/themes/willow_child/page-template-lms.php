<?php
/*
 * Template Name: Full Width lms data
 * Description: No Sidebar
 */
?>
<?php
  global $lms_wpdb;
  if ( !is_user_logged_in() )
  {
      wp_redirect( home_url() . '/wp-login.php?redirect_to='.urlencode( home_url().'/'.$_SERVER['REQUEST_URI'] ));
      exit();
  }

  $user_id = get_current_user_id();
  $user_meta = get_userdata($user_id); 
  $user_roles = $user_meta->roles; 
  if (!pmpro_hasMembershipLevel()) {
      wp_redirect( home_url() .'/wp-admin' );
      exit();
  }
  get_header(); 
?>

<div id="content" <?php post_class( 'visual-composer-page' ); ?>>
  <div class="section-anchor" data-anchor="remove" data-section="#header"></div>
<?php
  if(isset($_POST['submit'])) 
  {	
      //print_r($_POST);
    	$current_userid = get_current_user_id(); 
    	update_user_meta( $current_userid, 'lms-currency', $_POST['currency'] ); 
    	update_user_meta( $current_userid, 'lms-business-name', $_POST['business_project'] ); 
    	update_user_meta( $current_userid, 'lms-business-month', $_POST['business-month']);
    	update_user_meta( $current_userid, 'lms-business-year', $_POST['busniess-year'] ); 
    	//update_user_meta( $current_userid, 'lms-business-baseyear', $_POST['busniess-base-year'] );
    	update_user_meta( $current_userid, 'lms-business-taxpercent', $_POST['business_taxpercent'] );

      if(!empty($_POST['hdn_pro_id'])) 
      {
          $lms_wpdb->update ( 
            'tbl_project', 
            array ( 
              'name' => $_POST['business_project'],
              'currency' => $_POST['currency']
            ), 
            array ( 
              'user_id' => $current_userid,
              'id' => $_POST['hdn_pro_id'] 
            ), 
            array ( 
              '%s', // value1
              '%s'  // value2
            ), 
            array( '%d' )
          );
          if(empty($_POST['hdn_pro_id'])) {
              update_user_meta( $current_userid, 'lms-business-projectid', $_POST['hdn_pro_id'] );  
          } else {
              update_user_meta( $current_userid, 'lms-business-projectid', $_POST['hdn_pro_id'] );
          }

          //tbl_unforeseen data
          if(isset($_POST['hdn_unforeseen_percentage'])) {
            //SELECT `id`, `project_id`, `user_id`, `percent`, `promoters_capital`, `loan_capital`, `investors_financing`
            $lms_wpdb->update ( 
              'tbl_unforeseen', 
              array( 
                'project_id' => $_POST['hdn_pro_id'], 
                'percent' => $_POST['unforeseen_percentage']               
              ), 
              array( 
                'id' => $_POST['hdn_unforeseen_percentage'], 
                'user_id' => $current_userid 
              ), 
              array( 
                '%d', '%d'                
              ), 
              array( 
                '%d',
                '%d'
              )
            );
          } else {
              $ins_project = $lms_wpdb->insert ( 
                'tbl_unforeseen', 
                array ( 
                  'project_id' => $_POST['hdn_pro_id'], 
                  'user_id' => $current_userid,
                  'percent' => $_POST['unforeseen_percentage']
                ), 
                array ( 
                  '%d', 
                  '%d',
                  '%d'
                ) 
              );
          }

          //working_capital_months data
          if(isset($_POST['hdn_working_capital_months'])){
            $lms_wpdb->update ( 
              'tbl_capitalmonths', 
              array ( 
                'project_id' => $_POST['hdn_pro_id'], 
                'months'  => $_POST['working_capital_months']               
              ), 
              array ( 
                'id'      => $_POST['hdn_working_capital_months'], 
                'user_id' => $current_userid,
                'item'    => 'working capital'
              ), 
              array ( 
                '%d', '%d'                        
              ), 
              array ( 
                '%d', '%d', '%s' 
              )
            );
          } else {  
            $ins_project = $lms_wpdb->insert ( 
                'tbl_capitalmonths', 
                array ( 
                  'project_id' => $_POST['hdn_pro_id'], 
                  'user_id'    => $current_userid,
                  'months'     => $_POST['working_capital_months'],
                  'item'       => 'working capital'
                ), 
                array ( 
                  '%d', 
                  '%d',
                  '%d',
                  '%s'
                ) 
            );
          }

          //personnel_months data
          if(isset($_POST['hdn_personnel_months'])){
            $lms_wpdb->update ( 
              'tbl_capitalmonths', 
              array ( 
                'project_id' => $_POST['hdn_pro_id'], 
                'months'  => $_POST['personnel_months']               
              ), 
              array( 
                'id'      => $_POST['hdn_personnel_months'], 
                'user_id' => $current_userid,
                'item'    => 'personnel'
              ), 
              array( 
                '%d', '%d'                      
              ), 
              array( 
                '%d', '%d', '%s' 
              )
            );
          } else {  
            $ins_project = $lms_wpdb->insert ( 
                'tbl_capitalmonths', 
                array ( 
                  'project_id' => $_POST['hdn_pro_id'], 
                  'user_id'    => $current_userid,
                  'months'     => $_POST['personnel_months'],
                  'item'       => 'personnel'
                ), 
                array ( 
                  '%d', 
                  '%d',
                  '%d',
                  '%s'
                ) 
            );
          }

          if(isset($_POST['bsdm'])) {
            $cur_bsdm = $_POST['business-month'].'-'.$_POST['busniess-year'];   
            //2016-11-01         
            $new_date = $_POST['busniess-year'].'-'.sprintf("%02u", $_POST['business-month']).'-01';
            if($_POST['bsdm'] != $cur_bsdm) {

              //tbl_asset
              $asset_row = $lms_wpdb->get_var( "SELECT COUNT(*) FROM tbl_asset WHERE project_id = ".$_POST['hdn_pro_id'] );
              if($asset_row) {
                $lms_wpdb->update ( 
                  'tbl_asset', 
                  array( 'acquired_in' => $new_date ), 
                  array( 'project_id' => $_POST['hdn_pro_id'] ), 
                  array( '%s' ), 
                  array( '%d' ) 
                );
              } 

              //tbl_fixedcost
              $fixedcost_row = $lms_wpdb->get_var( "SELECT COUNT(*) FROM tbl_fixedcost WHERE project_id = ".$_POST['hdn_pro_id'] );  
              if($fixedcost_row){
                 $lms_wpdb->update ( 
                  'tbl_fixedcost', 
                  array( 'start_date' => $new_date ), 
                  array( 'project_id' => $_POST['hdn_pro_id'] ), 
                  array( '%s' ), 
                  array( '%d' ) 
                );
              }

              //tbl_loans
              $loans_row = $lms_wpdb->get_var( "SELECT COUNT(*) FROM tbl_loans WHERE project_id = 
                ".$_POST['hdn_pro_id'] );  
              if($fixedcost_row){
                $lms_wpdb->update ( 
                  'tbl_loans', 
                  array( 'start_date' => $new_date ), 
                  array( 'project_id' => $_POST['hdn_pro_id'] ), 
                  array( '%s' ), 
                  array( '%d' ) 
                );
              }

              //tbl_product_mode
              $product_mode_row = $lms_wpdb->get_var( "SELECT COUNT(*) FROM tbl_product_mode WHERE project_id = ".$_POST['hdn_pro_id'] );  
              if($fixedcost_row){
                $lms_wpdb->update ( 
                  'tbl_product_mode', 
                  array( 'start_month' => $new_date ), 
                  array( 'project_id' => $_POST['hdn_pro_id'] ), 
                  array( '%s' ), 
                  array( '%d' ) 
                );
              }
            } 
          }           
      } else {
       
          $ins_project = $lms_wpdb->insert ( 
            'tbl_project', 
            array ( 
              'user_id'  => $current_userid, 
              'name'     => $_POST['business_project'],
              'currency' => $_POST['currency']
            ), 
            array ( 
              '%d', 
              '%s',
              '%s'
            ) 
          );
          $new_project_id = $lms_wpdb->insert_id;
          update_user_meta( $current_userid, 'lms-business-projectid', $new_project_id );

          if(isset($_POST['hdn_unforeseen_percentage'])) {
            //SELECT `id`, `project_id`, `user_id`, `percent`, `promoters_capital`, `loan_capital`, `investors_financing`
            $lms_wpdb->update ( 
              'tbl_unforeseen', 
              array ( 
                'project_id' => $new_project_id, 
                'percent' => $_POST['unforeseen_percentage']               
              ), 
              array ( 
                'id'      => $_POST['hdn_unforeseen_percentage'], 
                'user_id' => $current_userid 
              ), 
              array (
                '%d', '%d'                      
              ), 
              array ( 
                '%d',
                '%d' 
              )
            );
          } else {
            $ins_project = $lms_wpdb->insert ( 
              'tbl_unforeseen', 
              array ( 
                'project_id' => $new_project_id, 
                'user_id'    => $current_userid,
                'percent'    => $_POST['unforeseen_percentage']
              ), 
              array ( 
                '%d', 
                '%d',
                '%d'
              ) 
            );
          }

          if(isset($_POST['hdn_working_capital_months'])) {
            $lms_wpdb->update ( 
              'tbl_capitalmonths',               
              array ( 
                'project_id' => $new_project_id, 
                'months'  => $_POST['working_capital_months']               
              ), 
              array ( 
                'id'      => $_POST['hdn_working_capital_months'], 
                'user_id' => $current_userid,
                'item'    => 'working capital'
              ), 
              array ( 
                '%d', '%d'                    
              ), 
              array (  
                '%d','%d','%s' 
              )
            );
          } else {  
            $ins_project = $lms_wpdb->insert ( 
                'tbl_capitalmonths', 
                array ( 
                  'project_id' => $new_project_id, 
                  'user_id'    => $current_userid,
                  'months'     => $_POST['working_capital_months'],
                  'item'       => 'working capital'
                ), 
                array ( 
                  '%d', 
                  '%d',
                  '%d',
                  '%s'
                ) 
            );
          }

          //personnel_months data
          if(isset($_POST['hdn_personnel_months'])){
            $lms_wpdb->update ( 
              'tbl_capitalmonths', 
              array ( 
                'project_id' => $new_project_id, 
                'months'  => $_POST['personnel_months']               
              ), 
              array( 
                'id'      => $_POST['hdn_personnel_months'], 
                'user_id' => $current_userid,
                'item'    => 'personnel'
              ), 
              array( 
                '%d', '%d'                        
              ), 
              array( 
                '%d','%d','%s' 
              )
            );
          } else {  
            $ins_project = $lms_wpdb->insert ( 
                'tbl_capitalmonths', 
                array ( 
                  'project_id' => $new_project_id, 
                  'user_id'    => $current_userid,
                  'months'     => $_POST['personnel_months'],
                  'item'       => 'personnel'
                ), 
                array ( 
                  '%d', 
                  '%d',
                  '%d',
                  '%s'
                ) 
            );
          }

          $cur_bsdm = $_POST['business-month'].'-'.$_POST['busniess-year'];   
          //2016-11-01         
          $new_date = $_POST['busniess-year'].'-'.sprintf("%02u", $_POST['business-month']).'-01';         
          //tbl_asset
          $asset_row = $lms_wpdb->get_var( "SELECT COUNT(*) FROM tbl_asset WHERE project_id = ".$new_project_id );
          if($asset_row) {
            $lms_wpdb->update ( 
              'tbl_asset', 
              array( 'acquired_in' => $new_date ), 
              array( 'project_id' => $new_project_id ), 
              array( '%s' ), 
              array( '%d' ) 
            );
          } 
          
          //tbl_fixedcost
          $fixedcost_row = $lms_wpdb->get_var( "SELECT COUNT(*) FROM tbl_fixedcost WHERE project_id = ".$new_project_id );  
          if($fixedcost_row){
             $lms_wpdb->update ( 
              'tbl_fixedcost', 
              array( 'start_date' => $new_date ), 
              array( 'project_id' => $new_project_id ), 
              array( '%s' ), 
              array( '%d' ) 
            );
          }

          //tbl_loans
          $loans_row = $lms_wpdb->get_var( "SELECT COUNT(*) FROM tbl_loans WHERE project_id = ".$new_project_id  );
          if($fixedcost_row){
            $lms_wpdb->update ( 
              'tbl_loans', 
              array( 'start_date' => $new_date ), 
              array( 'project_id' => $new_project_id ), 
              array( '%s' ), 
              array( '%d' ) 
            );
          }

          //tbl_product_mode
          $product_mode_row = $lms_wpdb->get_var( "SELECT COUNT(*) FROM tbl_product_mode WHERE project_id = ".$new_project_id );  
          if($fixedcost_row){
            $lms_wpdb->update ( 
              'tbl_product_mode', 
              array( 'start_month' => $new_date ), 
              array( 'project_id'  => $new_project_id ), 
              array( '%s' ), 
              array( '%d' ) 
            );
          }                        
      }
      
      update_user_meta( $current_userid, 'lms-business-userrole', 'basic' );
    	echo "<center><h4>Business profile updated successfully</h4></center>";
  }

  $meta = get_user_meta( $user_id );
  //print_r($meta);
  $lms_currency = get_user_meta($user_id, 'lms-currency', true);
  $lms_business_name = get_user_meta($user_id, 'lms-business-name', true);
  $lms_business_month = get_user_meta($user_id, 'lms-business-month', true);
  $lms_business_year = get_user_meta($user_id, 'lms-business-year', true);
  //$lms_business_baseyear = get_user_meta($user_id, 'lms-business-baseyear', true);
  $lms_business_taxpercent = get_user_meta($user_id, 'lms-business-taxpercent', true);
  if(isset($_POST['hdn_pro_id'])) {
    if(!empty($_POST['hdn_pro_id'])){
      $lms_business_projectid = $_POST['hdn_pro_id'];
    } else {
      $lms_business_projectid = get_user_meta($user_id, 'lms-business-projectid', true);
    }
  } else {
    $lms_business_projectid = $_GET['pid'];
  }
  

$lms_business_userrole = get_user_meta($user_id, 'lms-business-userrole', true);


/*echo 'lms_currency '.$lms_currency; 
echo '<br>';
echo 'lms_business_name '.$lms_business_name;
echo '<br>';
echo 'lms_business_month '.$lms_business_month;
echo '<br>';
echo 'lms_business_year '.$lms_business_year;
echo '<br>';
echo 'lms_business_taxpercent '.$lms_business_taxpercent;
echo '<br>';
echo 'lms_business_projectid '.$lms_business_projectid;
*/
  if(!empty($lms_currency) && 
    !empty($lms_business_name) && 
    !empty($lms_business_month) && 
    !empty($lms_business_year) && 
    !empty($lms_business_taxpercent) && 
    !empty($lms_business_projectid) )
  {
    $userdata = get_userdata( $user_id );
    $user_img_arr = get_user_meta($user_id, "basic_user_avatar");
   
    if(empty($user_img_arr)){
      $user_img_arr[0]["full"] = 'http://1.gravatar.com/avatar/7eddb39c5e3dd4ea291558c62b2b2b2c?s=96&d=http%3A%2F%2Fbizskillsacademy.com%2Fwp-content%2Fuploads%2F2016%2F06%2Fapple-icon-152x152.png&r=g';
    }
?>
    <form class="login-wrapper" id="login_form" action="https://lms.bizskillsacademy.com/site/wplogin" method="post">
      <input type="hidden" name="hdn_profile_pic" value="<?php echo $user_img_arr[0]["full"]; ?>" /> 
      <input type="hidden" name="hdn_user_email" value="<?php echo $userdata->user_email; ?>" /> 
      <input type="hidden" name="username" value="<?php echo $userdata->user_login; ?>" />
      <input type="hidden" name="password" value="<?php echo $userdata->user_pass; ?>" />
      <input type="hidden" name="hdn_user_id" value="<?php echo $userdata->ID; ?>" /> 
      <input type="hidden" name="hdn_lms_data[currency]" value="<?php echo $lms_currency; ?>" /> 
      <input type="hidden" name="hdn_lms_data[business_name]" 
      value="<?php echo $lms_business_name; ?>" />
      <input type="hidden" name="hdn_lms_data[business_month]" 
      value="<?php echo $lms_business_month; ?>" /> 
      <input type="hidden" name="hdn_lms_data[business_year]" 
      value="<?php echo $lms_business_year; ?>" />
      <input type="hidden" name="hdn_lms_data[business_projectid]" 
      value="<?php echo $lms_business_projectid; ?>" />
      <input type="hidden" name="hdn_lms_data[business_taxpercent]" 
      value="<?php echo $lms_business_taxpercent; ?>"/> 
      <input type="hidden" name="hdn_lms_data[business_userrole]" 
      value="<?php echo $lms_business_userrole; ?>"/>          
    </form>
    <script type="text/javascript">
      jQuery(document).ready(function( $ ) {
        $('#currency option[value="<?php echo $lms_currency; ?>"]').attr("selected", "selected");
        $('#business_month option[value="<?php echo $lms_business_month; ?>"]').attr("selected", "selected");
        $('#busniess_year option[value="<?php echo $lms_business_year; ?>"]').attr("selected","selected");
        <?php 
          if(isset($_POST['submit'])) {
        ?>           
        <?php
            if(!isset($_SESSION['lms_referer'])) {              
        ?>           
            $('#login_form').submit(); 
        <?php
            } else {             
              echo 'location.assign("'.$_SESSION['lms_referer'].'");';
            }
        ?>            
        <?php           
          }
        ?>       
        $( ".lmslogin" ).click(function() { });
      });
    </script>    
    <style type="text/css">.lmslogin{}</style>
<?php    
} else {
    echo '<center><p style="text-align: center;">please fill the information</p></center>';       
    echo '<script type="text/javascript">
      jQuery(document).ready(function( $ ) {
         $("#business_month option[value="'.date('n').'"]").attr("selected", "selected");
      }
      </script>';
}
?>
  <script type="text/javascript">
    jQuery(document).ready(function( $ ) {

        $( "#lms_data" ).submit(function( event ) {
            if($('#currency').val()=='0'){
              $('#currency').focus();
              return false;
            }             
            else if($('#business_taxpercent').val()=='') {
              $('#business_taxpercent').focus();
              return false;
            }
            else if($('#business_project').val()=='') {
              $('#business_project').focus()
              return false;
            }
            else if($('#business_month').val()=='') {
              $('#business_month').focus();
              return false;
            }
            else if($('#busniess_year').val()=='') {
              $('#busniess_year').focus();
              return false;
            }           
            else {
              return true;
            }                                  
        });
      
        $( "#business_month" ).change(function() {
          var cur_m_index = $("#business_month option[value='<?php echo date('n'); ?>']").index();
          if($('#busniess_year :selected').text() == <?php echo date("Y");?>) { 
            if(cur_m_index > $(this).children('option:selected').index()){
              alert('Please select month <?php echo date('F');?> or after <?php echo date('F');?>.')
              $("#business_month option[value='<?php echo date('n'); ?>']").attr("selected", "selected");
            }            
          }        
        });
        $( "#busniess_year" ).change(function() {
          $("#business_month").trigger("change");
        });
        $( "#dlst_business_project" ).change(function() { 

          if($(this).val()=='newproject') {
            $('#business_project').val('').attr("placeholder", "Create New Project");   
            $('#hdn_pro_id').val('');       
          } else {
            $('#business_project').val($('#dlst_business_project option:selected').text()).attr("placeholder", "Update Project");
            $('#hdn_pro_id').val($(this).val());
          }
          $('#business_project').show()
        });        
        $('#dlst_business_project option[value="<?php echo $_GET['pid']; ?>"]').attr("selected","selected");
        $("#dlst_business_project").trigger("change");
    });
  </script>
  <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
  <div class="container business_profile">
    <form action="" method="post" name="lms_data" id="lms_data">
      <div class="form_row">
        <div class="col-md-6 text-right">
          <label> Please Select Currency :</label>
        </div>
        <div class="col-md-6">
          <select name="currency" id="currency" class="currency">
            <option value="0">Please Select</option>
            <option value="£Sd">£Sd</option><option value="₱">₱</option><option value="₭">₭</option><option value="₦">₦</option><option value="₩">₩</option><option value="₩">₩</option><option value="₮">₮</option><option value="€">€</option><option value="฿">฿</option><option value="₡">₡</option><option value="৳">৳</option><option value="៛">៛</option><option value="₲">₲</option><option value="₴">₴</option><option value="₵">₵</option><option value="₪">₪</option><option value="₫">₫</option><option value="AED">AED</option><option value="ALL">ALL</option><option value="AMD">AMD</option><option value="ANG">ANG</option><option value="AOA">AOA</option><option value="AR$">AR$</option><option value="ARS">ARS</option><option value="AU$">AU$</option><option value="AUD">AUD</option><option value="AWG">AWG</option><option value="AZN">AZN</option><option value="B$">B$</option><option value="BAM">BAM</option><option value="BBD">BBD</option><option value="BD$">BD$</option><option value="Bds$">Bds$</option><option value="BDT">BDT</option><option value="BGN">BGN</option><option value="BHD">BHD</option><option value="BIF">BIF</option><option value="BMD">BMD</option><option value="BOB">BOB</option><option value="Br">Br</option><option value="BRL">BRL</option><option value="Bs">Bs</option><option value="BS$">BS$</option><option value="BSD">BSD</option><option value="BTN">BTN</option><option value="BWP">BWP</option><option value="BYN">BYN</option><option value="BZ$">BZ$</option><option value="BZD">BZD</option><option value="C$">C$</option><option value="CA$">CA$</option><option value="CAD">CAD</option><option value="CDF">CDF</option><option value="CDFr">CDFr</option><option value="CFA">CFA</option><option value="CFP">CFP</option><option value="CHF">CHF</option><option value="CI$">CI$</option><option value="CL$">CL$</option><option value="CLP">CLP</option><option value="CN¥">CN¥</option><option value="CNY">CNY</option><option value="Col$">Col$</option><option value="COP">COP</option><option value="CRC">CRC</option><option value="CU$">CU$</option><option value="CUP">CUP</option><option value="CV$">CV$</option><option value="CVE">CVE</option><option value="CY£">CY£</option><option value="CZK">CZK</option><option value="D">D</option><option value="Db">Db</option><option value="DJF">DJF</option><option value="DKK">DKK</option><option value="DOP">DOP</option><option value="DZD">DZD</option><option value="EC$">EC$</option><option value="EGP">EGP</option><option value="ERN">ERN</option><option value="ETB">ETB</option><option value="EUR">EUR</option><option value="ƒ">ƒ</option><option value="FBu">FBu</option><option value="Fdj">Fdj</option><option value="FG">FG</option><option value="FJ$">FJ$</option><option value="FJD">FJD</option><option value="FK£">FK£</option><option value="FKP">FKP</option><option value="franc">franc</option><option value="Ft">Ft</option><option value="G">G</option><option value="GBP">GBP</option><option value="GEL">GEL</option><option value="GGP">GGP</option><option value="GHS">GHS</option><option value="GI£">GI£</option><option value="GIP">GIP</option><option value="GMD">GMD</option><option value="GNF">GNF</option><option value="GTQ">GTQ</option><option value="GY$">GY$</option><option value="GYD">GYD</option><option value="HK$">HK$</option><option value="HKD">HKD</option><option value="HNL">HNL</option><option value="HRK">HRK</option><option value="HTG">HTG</option><option value="HUF">HUF</option><option value="IDR">IDR</option><option value="ILS">ILS</option><option value="IMP">IMP</option><option value="IN₨">IN₨</option><option value="INR">INR</option><option value="IQD">IQD</option><option value="IRR">IRR</option><option value="ISK">ISK</option><option value="JA$">JA$</option><option value="JD">JD</option><option value="JEP">JEP</option><option value="JMD">JMD</option><option value="JOD">JOD</option><option value="JP¥">JP¥</option><option value="JPY">JPY</option><option value="K">K</option><option value="Kč">Kč</option><option value="KES">KES</option><option value="KGS">KGS</option><option value="KHR">KHR</option><option value="KM">KM</option><option value="KMF">KMF</option><option value="KPW">KPW</option><option value="KR">KR</option><option value="KRW">KRW</option><option value="KSh">KSh</option><option value="KWD">KWD</option><option value="KYD">KYD</option><option value="Kz">Kz</option><option value="KZT">KZT</option><option value="L">L</option><option value="L$">L$</option><option value="LAK">LAK</option><option value="LBP">LBP</option><option value="Le">Le</option><option value="LK₨">LK₨</option><option value="LKR">LKR</option><option value="Lm">Lm</option><option value="LRD">LRD</option><option value="Ls">Ls</option><option value="LSL">LSL</option><option value="Lt">Lt</option><option value="LYD">LYD</option><option value="m">m</option><option value="MAD">MAD</option><option value="MDL">MDL</option><option value="Mex$">Mex$</option><option value="MGA">MGA</option><option value="MK">MK</option><option value="MKD">MKD</option><option value="MMK">MMK</option><option value="MNT">MNT</option><option value="MO$">MO$</option><option value="MOP">MOP</option><option value="MRf">MRf</option><option value="MRO">MRO</option><option value="MTn">MTn</option><option value="MU₨">MU₨</option><option value="MUR">MUR</option><option value="MVR">MVR</option><option value="MWK">MWK</option><option value="MXN">MXN</option><option value="MYR">MYR</option><option value="MZN">MZN</option><option value="N$">N$</option><option value="NAD">NAD</option><option value="NAƒ">NAƒ</option><option value="Nfk">Nfk</option><option value="NGN">NGN</option><option value="NIO">NIO</option><option value="NOK">NOK</option><option value="NOK">NOK</option><option value="none">none</option><option value="NP₨">NP₨</option><option value="NPR">NPR</option><option value="NT$">NT$</option><option value="Nu">Nu</option><option value="NZ$">NZ$</option><option value="NZD">NZD</option><option value="ø">ø</option><option value="OMR">OMR</option><option value="øre">øre</option><option value="P">P</option><option value="PAB">PAB</option><option value="PEN">PEN</option><option value="PGK">PGK</option><option value="PHP">PHP</option><option value="PKR">PKR</option><option value="PKRs">PKRs</option><option value="PLN">PLN</option><option value="PT$">PT$</option><option value="PYG">PYG</option><option value="Q">Q</option><option value="QAR">QAR</option><option value="R$">R$</option><option value="RD$">RD$</option><option value="RF">RF</option><option value="RM">RM</option><option value="ROL">ROL</option><option value="RON">RON</option><option value="Rp">Rp</option><option value="RSD">RSD</option><option value="RUB">RUB</option><option value="RUруб">RUруб</option><option value="RWF">RWF</option><option value="S$">S$</option><option value="S./">S./</option><option value="S£">S£</option><option value="SAR">SAR</option><option value="SAR">SAR</option><option value="SBD">SBD</option><option value="SCR">SCR</option><option value="SDG">SDG</option><option value="SEK">SEK</option><option value="SGD">SGD</option><option value="Sh">Sh</option><option value="SH£">SH£</option><option value="SHP">SHP</option><option value="SI$">SI$</option><option value="Sk">Sk</option><option value="SLL">SLL</option><option value="SOS">SOS</option><option value="SR$">SR$</option><option value="SRD">SRD</option><option value="SRe">SRe</option><option value="SSP">SSP</option><option value="STD">STD</option><option value="SYP">SYP</option><option value="SZL">SZL</option><option value="THB">THB</option><option value="TJS">TJS</option><option value="TJS">TJS</option><option value="TMT">TMT</option><option value="TND">TND</option><option value="TOP">TOP</option><option value="TRY">TRY</option><option value="TSh">TSh</option><option value="TT$">TT$</option><option value="TTD">TTD</option><option value="TWD">TWD</option><option value="TZS">TZS</option><option value="UAH">UAH</option><option value="UGX">UGX</option><option value="UK£">UK£</option><option value="UM">UM</option><option value="UR$">UR$</option><option value="US$">US$</option><option value="USD">USD</option><option value="USh">USh</option><option value="UYU">UYU</option><option value="UZS">UZS</option><option value="UZS">UZS</option><option value="VEF">VEF</option><option value="VND">VND</option><option value="Vt">Vt</option><option value="VUV">VUV</option><option value="WS$">WS$</option><option value="WST">WST</option><option value="XAF">XAF</option><option value="XCD">XCD</option><option value="XDR">XDR</option><option value="XOF">XOF</option><option value="XPF">XPF</option><option value="YER">YER</option><option value="YTL">YTL</option><option value="Z$">Z$</option><option value="ZAR">ZAR</option><option value="ZK">ZK</option><option value="zł">zł</option><option value="ZMW">ZMW</option><option value="дин">дин</option><option value="лв">лв</option><option value="ман">ман</option><option value="դր">դր</option><option value="ب.د">ب.د</option><option value="ج.م">ج.م</option><option value="د.إ">د.إ</option><option value="د.ت">د.ت</option><option value="د.ك">د.ك</option><option value="د.م">د.م</option><option value="دج">دج</option><option value="ر.س">ر.س</option><option value="ر.ع">ر.ع</option><option value="ر.ق">ر.ق</option><option value="ريال">ريال</option><option value="ع.د">ع.د</option><option value="ل.د">ل.د</option><option value="ل.ل">ل.ل</option></select>                       
        </div>
        <div class="clearfix"></div>
      </div>      
      
      <div class="form_row">
          <div class="col-md-6 text-right"> <label> Tax % : </label></div>
          <div class="col-md-6"> 
          <input type="text" name="business_taxpercent" id="business_taxpercent" required  value="<?php echo $lms_business_taxpercent ?>"></div>
          <div class="clearfix"></div>
      </div>     
      <?php
          $unforeseen_percentage_arr = $lms_wpdb->get_results( 'SELECT * FROM tbl_unforeseen where user_id = '.$user_id.' LIMIT 1', OBJECT ); 
          if(!empty($unforeseen_percentage_arr)) {
          echo '<input type="hidden" name="hdn_unforeseen_percentage" id="hdn_unforeseen_percentage" 
                 value="'.$unforeseen_percentage_arr[0]->id.'">';
          echo '<input type="hidden" name="unforeseen_percentage" id="unforeseen_percentage" value="'.$unforeseen_percentage_arr[0]->percent.'">';
          } else {
            echo '<input type="hidden" name="unforeseen_percentage" id="unforeseen_percentage" value="1"/>';
          }
      ?>                      
      <?php
          $working_capital_months_arr = $lms_wpdb->get_results( 'SELECT * FROM tbl_capitalmonths where user_id = '.$user_id.' AND item = "working capital" LIMIT 1', OBJECT ); 
          if(!empty($working_capital_months_arr)) {
              echo '<input type="hidden" name="hdn_working_capital_months" id="hdn_working_capital_months" 
                  value="'.$working_capital_months_arr[0]->id.'">';
              echo '<input type="hidden" name="working_capital_months" id="working_capital_months"  
                  value="'.$working_capital_months_arr[0]->months.'" >';
          } else {
              echo '<input type="hidden" name="working_capital_months" id="working_capital_months" value="1" />';
          }
      ?>
      <?php
          $personnel_months_arr = $lms_wpdb->get_results( 'SELECT * FROM tbl_capitalmonths 
          where user_id = '.$user_id.' AND item = "personnel" LIMIT 1', OBJECT ); 
          if(!empty($personnel_months_arr)) {
            echo '<input type="hidden" name="hdn_personnel_months" id="hdn_personnel_months" 
            value="'.$personnel_months_arr[0]->id.'">';
            echo '<input type="hidden" name="personnel_months" id="personnel_months"  
            value="'.$personnel_months_arr[0]->months.'">';
          } else {
              echo '<input type="hidden" name="personnel_months" id="personnel_months" value="1" />';
          }
      ?>            
      <div class="form_row">
        <div class="col-md-6 text-right"> <label> Business Name : </label></div>
        <div class="col-md-6"> 
        <select id="dlst_business_project" name="dlst_business_project">
          <option value="newproject">Create New Project</option>
        <?php
          $project_arr = $lms_wpdb->get_results( 'SELECT id, user_id, name, currency FROM tbl_project where user_id = '.$user_id, OBJECT );  
            if(!empty($project_arr)) {
              update_user_meta( $user_id, 'lms-business-projectid', $project_arr[0]->id );     
              foreach ( $project_arr as $project_row ) 
              {
                 echo '<option value="'.$project_row->id.'">'.$project_row->name.'</option>';
              }       
             
            } else {              
              //echo '<input type="text" name="business_project" id="business_project" required />';
            }
        ?>        
         </select> <br>
         <input type="text" name="business_project" id="business_project" value="" 
         style="display: none" />
         <input type="hidden" name="hdn_pro_id" id="hdn_pro_id" value="" />
       </div>
       <div class="clearfix"></div>
      </div>
      <div class="form_row">
       <div class="col-md-6 text-right"> <label> Business startup month and year :</label></div>
       <div class="col-md-6">
        <select name="business-month" id="business_month">
          <option value=""> - Month - </option>
          <option value="1">January</option>
          <option value="2">Febuary</option>
          <option value="3">March</option>
          <option value="4">April</option>
          <option value="5">May</option>
          <option value="6">June</option>
          <option value="7">July</option>
          <option value="8">August</option>
          <option value="9">September</option>
          <option value="10">October</option>
          <option value="11">November</option>
          <option value="12">December</option>
        </select>
        <select name="busniess-year" id="busniess_year">
          <option value=""> - Year - </option>
          <?php 
      		for($i=date("Y"); $i<=2050; $i++){
            if($lms_business_year == $i){
              echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
            } else{
              echo '<option value="'.$i.'">'.$i.'</option>';
            }
      		} 
          ?>
        </select>
        <?php 
          if(!empty($lms_business_month) && !empty($lms_business_year)){      
            echo '<input type="hidden" name="bsdm" 
            value="'.$lms_business_month.'-'.$lms_business_year.'" />';
          }
        ?>        
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="form_row">
        <!-- div class="col-md-6 text-right"><label> Choose your base year * </label></div>
        <div class="col-md-6 ">
        <select id="busniess_base_year" name="busniess-base-year">
          <option value=""> - Year - </option>
          <?php 
        		/*for($i=date("Y"); $i<=2050; $i++){
              if(lms_business_baseyear == $i){
                echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
              } else {
                echo '<option value="'.$i.'">'.$i.'</option>';
              }
        		}*/
          ?>
        </select>
        </div -->
        <div class="clearfix"></div>
        <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
        <div class="col-md-12 text-center">       
          <input class="button button-primary" type="submit" value="submit" name="submit">
        </div>
      </div>
    </form>
  </div>
  <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span></div>
  <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span></div>
</div>
<?php get_footer(); ?>