<?php
/**
 * The Template for displaying all single publications.
 *
 * @package WP Publication Archive
 * @since 3.0
 */

get_header();
global $pagename;
$hero_title = rwmb_meta( 'bizskills-hero_title' );
$content = rwmb_meta( 'bizskills-hero_content' );
$background_image2 = 'https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/04/banner-img.jpg';
$background_image = rwmb_meta( 'bizskills-image_advanced_2' );
if(!empty($background_image))
{
	foreach ( $background_image as $bg ) {
    	$background_image =  $bg['full_url'];
	}
}
else
{
	$background_image = $background_image2;

}	
 ?>
<style>
[data-class="hide"] {
    display:none;
}
[data-class="show"] {
    display:block;
}
.hero-image {
  background-image: url("<?php echo $background_image; ?>");
   height: 500px;
  background-position: center -20px;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  ;
}
.content_keywords_single_publication
{
	margin-bottom:80px;
}
.hero-text
{
	top:47%;
}
</style>
<div class="hero-image">
  <div class="hero-text">
    	<h2><?php if(empty($hero_title)) { strtoupper(single_post_title());  } else{echo $hero_title;}  ?></h2>

  <?php if(!empty($content)) { ?>
    <p><?php echo $content; ?></p>
    <?php } ?>
  </div>
</div>
<div class="single-publication-custom-sp">
	<div id="primary" class="site-content custom-publication_page ">
		<div id="content" role="main">

			<?php
			while ( have_posts() ) : the_post(); $publication = new WP_Publication_Archive_Item( get_the_ID() ); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php $publication->the_thumbnail(); ?>
						<div class="single_publication_content_top"><?php the_content(); ?></div>
						<section class="publication-downloads">
							<?php $publication->the_uri_single_publication(); ?>
							<?php /*$publication->list_downloads(); */ ?>
						</section>
						<div class="content_keywords_single_publication">
						<div class="summery_div_single_publication">
							<?php echo "<span class='summery_single_pub'>Summary:</span> <span class='summery_single_content'>".strip_tags(get_the_content()); ?></span>
						</div>
						<footer class="entry-meta keyword_single_page">
							<?php strip_tags($publication->the_keywords());	 ?>
							<?php $publication->the_categories(); ?>
							<?php edit_post_link( __( 'Edit Publication', 'wp_pubarch_translate' ), '<span class="edit-link">', '</span>' ); ?>
						</footer><!-- .entry-meta -->
						</div>
					</div><!-- .entry-content -->

					
				</article><!-- #post -->

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_sidebar(); ?> 
<div class="clear"></div>	
</div>	

<?php get_footer(); ?>