<style>
.single_comment
{
	margin-bottom:20px;
}
.ca_custom
{
	border:1px solid #e4e4d4;
	background-color:#fff;
}
.content-post
{
	margin-bottom:40px;
}

</style>
<div class="blog-loop">

	<?php if ( have_posts() ) : while( have_posts() ) : the_post() ; ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class( 'content-post' ); ?>>

			<?php if ( has_post_thumbnail() ) : ?>
				
				<div class="post-thumbnail">

					<img src="<?php echo willow_aq_resize( get_post_thumbnail_id(), 790, 500, true, true ); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" />

				</div>

			<?php else : ?>

				<div class="post-thumbnail blank"></div>

			<?php endif; ?>

			</div>
			<h2 class="post-title custom_title_single_blog">

				<?php if ( ! is_single( get_the_ID() ) ) : ?>

					<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>

				<?php else : ?>

					<?php the_title(); ?>

				<?php endif; ?>

			</h2>
			

			<div class="post-content custom-single-blog-border">
			<div class="custom-single-blog-border_p">
				<?php ob_start(); ?>
				<p class="read-more"><a href="<?php the_permalink(); ?>" class="btn btn-black"><?php _e( 'Read More', 'willow' ); ?></a></p>
				<?php $read_more = ob_get_clean(); ?>
				<?php the_content( $read_more ); ?>
				</div>
			</div>
			<?php  

			$post_id = get_the_ID(); 
			$comments = get_comments( array( 'post_id' => $post_id ) );
			
 			if(!empty($comments))
 			{
 		?>
 			 			<div class="comment_area">

 			 				<div class="ca_data_custom">	
	 			 			<h3> <strong>All Comments</strong></h3>
	 			 			<?php 
	 			 			foreach($comments as $data) {
	 			 				$date = $data->comment_date;	 			
	 			 			?>
		 			 			<div class="single_comment">
			 			 			<div class="col-md-2 _avatar col-sm-2 col-xs-2">			 			 	
			 			 				<?php
			 			 				$user = get_user_by( 'email', $data->comment_author_email );
			 			 				if($user)
										{										   
										   	echo get_avatar( $user->ID, 55 );
										} else {
											echo get_avatar( $data->comment_author_email, 55 );
										}
			 			 				?>
			 			 			</div>
			 			 			<div class="col-md-10 ca_custom col-sm-10 col-xs-10">
			 			 				<h6>
				 			 				<strong><?php echo $data->comment_author; ?></strong>
				 			 				<span class="comment-date">
				 			 					<?php echo date("m.d.Y", strtotime($date)); ?>
				 			 				</span>
			 			 				</h2>
			 			 				<p><?php echo $data->comment_content; ?></p>
			 			 			</div>
			 			 		</div>
	 			 			<?php } ?>
	 			 			<div style="clear: both;"></div>
	 			 			</div>
	 			 			<div style="clear: both;"></div>
 			 			</div>

 			<?php	
 			}

 			?>


			<?php if ( is_single() ) : ?>



					<?php wp_link_pages(array(

						'before' => '<p class="post-pagination">',

						'after ' => '</p>',

						'link_before' => '',

						'link_after' => '',

						'next_or_number' => 'next',

						'nextpagelink' => '<span class="next">' . __( 'Next Page &raquo;', 'willow' ) . '</span>',

						'previouspagelink' => '<span class="prev">' . __( '&laquo; Previous Page', 'willow' ) . '</span>',

					)); ?>



				<div class="post-tags tagcloud">

					<?php the_tags( '', '', '' ); ?>

				</div>

			<?php endif; ?>


</div>
<div class="clear"></div>
		</article>
<div class="clear"></div>


	<?php endwhile; endif; ?>

</div>	