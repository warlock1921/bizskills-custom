	<?php get_header(); 
	global $pagename;
$hero_title = rwmb_meta( 'bizskills-hero_title' );
$content = rwmb_meta( 'bizskills-hero_content' );
$background_image2 = 'https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/04/banner-img.jpg';
$background_image = rwmb_meta( 'bizskills-image_advanced_2' );
if(!empty($background_image))
{
	foreach ( $background_image as $bg ) {
    	$background_image =  $bg['full_url'];
	}
}
else
{
	$background_image = $background_image2;

}	
 ?>
<style>
[data-class="hide"] {
    display:none;
}
[data-class="show"] {
    display:block;
}
.hero-image {
  background-image: url("<?php echo $background_image; ?>");
   height: 500px;
  background-position: center -20px;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  ;
}
.hero-text
{
	top:47%;
}
</style>
<div class="hero-image">
  <div class="hero-text">
    	<h2><?php if(empty($hero_title)) { strtoupper(single_post_title());  } else{echo $hero_title;}  ?></h2>

  <?php if(!empty($content)) { ?>
    <p><?php echo $content; ?></p>
    <?php } ?>
  </div>
</div>
	<div class="single-blog-custom-sp">

	<section id="content" class="content-section section ">

		<div class="sec_data">
		<div class="container container-table custom_left">
		
		<div class=" container-table">
			<div class="main-section" role="main">

				<?php get_template_part( 'loop1' ); ?>
			<?php comments_template(); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
		</div>
		</div>
		
	</section>
	</div>
<div class="clear"></div>

	<?php get_footer(); ?>