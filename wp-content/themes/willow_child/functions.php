<?php
add_action('init', 'myStartSession', 1);
add_action('wp_logout', 'myEndSession');
add_action('wp_login', 'myEndSession');
add_action( 'widgets_init', 'theme_slug_widgets_init' );
add_action( 'wp_enqueue_scripts', 'add_my_script' );

function add_my_script() {
    wp_enqueue_script(
        'your-script', // name your script so that you can attach other scripts and de-register, etc.
        get_stylesheet_directory_uri() . '/js/custom.js', // this is the location of your script file
        array('jquery') // this array lists the scripts upon which your script depends
    );
}
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'comment_sidebar', 'theme-slug' ),
        'id' => 'sidebar-comment',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<div class="comment_area">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}
/*custom code added to remove quantity */
add_filter( 'woocommerce_is_sold_individually', 'wc_remove_all_quantity_fields', 10, 2 );
function wc_remove_all_quantity_fields( $return, $product ) {
    return( true );
}
function courses_func() {
	// First lets set some arguments for the query:
	// Optionally, those could of course go directly into the query,
	// especially, if you have no others but post type.
	$gold_cat = 169;
	$bronze_cat = 167;
	$silver_cat = 168;
	$platinum_cat = 170;

	$args = array(
	    'post_type' => 'sfwd-courses',
	    'posts_per_page' => 4,
	    'tax_query' => array(
            array(
                'taxonomy' => 'ld_course_category',
                'field' => 'term_id',
                'terms' => $bronze_cat,
            )
        )
	    // Several more arguments could go here. Last one without a comma.
	);

	// Query the posts:
	$obituary_query = new WP_Query($args);

	// Loop through the obituaries:
	echo '<div class="col-md-3  col-sm-6 bronze_sec common_course_Class">
	<h4 class="course_head">Bronze Certificate</h4>
	';
	while ($obituary_query->have_posts()) : $obituary_query->the_post();
		$id = get_the_ID();
	    $permalink  = get_the_permalink();
	    $data = get_post_taxonomies( $post );
	    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
	    ?>
	   <div class="course_custom_container">
		  <img src="<?php echo $featured_img_url; ?>" alt="Avatar" class="course_image">
		      <div class="text_course course_title"><b class=""><?php echo get_the_title(); ?></b></div>

		  <div class="custom_course_overlay">
		    <div class="text_course"><a href="<?php echo $permalink; ?>"><u>Click to see lessons</u></a></div>
		  </div>
		</div>
	    <div class="custom_bottom_gap"></div>
	  <?php
	    /*echo '<li><a href='.$permalink.'>'.get_the_title().'</a></li>'; */
	endwhile;
echo '</div>';
	// Reset Post Data
	wp_reset_postdata();

	$args = array(
	    'post_type' => 'sfwd-courses',
	    'posts_per_page' => 4,
	    'tax_query' => array(
            array(
                'taxonomy' => 'ld_course_category',
                'field' => 'term_id',
                'terms' => $silver_cat,
            )
        )
	    // Several more arguments could go here. Last one without a comma.
	);

	// Query the posts:
	$obituary_query = new WP_Query($args);

	// Loop through the obituaries:
	echo '<div class="col-md-3 col-sm-6 silver_sec common_course_Class">
			<h4 class="course_head">Silver Certificate</h4>

	';
	while ($obituary_query->have_posts()) : $obituary_query->the_post();
		$id = get_the_ID();
	    $permalink  = get_the_permalink();
	    $data = get_post_taxonomies( $post );
	    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
	    ?>
	   <div class="course_custom_container">
		  <img src="<?php echo $featured_img_url; ?>" alt="Avatar" class="course_image">
		      <div class="text_course course_title"><b><?php echo get_the_title(); ?></b></div>

		  <div class="custom_course_overlay">
		    <div class="text_course"><a href="<?php echo $permalink; ?>"><u>Click to see lessons</u></a></div>
		  </div>
		</div>
	    <div class="custom_bottom_gap"></div>
	  <?php
	    /*echo '<li><a href='.$permalink.'>'.get_the_title().'</a></li>'; */
	endwhile;
echo '</div>
';
	// Reset Post Data
	wp_reset_postdata();


	$args = array(
	    'post_type' => 'sfwd-courses',
	    'posts_per_page' => 4,
	    'tax_query' => array(
            array(
                'taxonomy' => 'ld_course_category',
                'field' => 'term_id',
                'terms' => $gold_cat,
            )
        )
	    // Several more arguments could go here. Last one without a comma.
	);

	// Query the posts:
	$obituary_query = new WP_Query($args);

	// Loop through the obituaries:
	echo '<div class="col-md-3 col-sm-6 gold_sec common_course_Class">
			<h4 class="course_head">Gold Certificate</h4>';

	while ($obituary_query->have_posts()) : $obituary_query->the_post();
		$id = get_the_ID();
	    $permalink  = get_the_permalink();
	    $data = get_post_taxonomies( $post );
	    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
	    ?>
	   <div class="course_custom_container">
		  <img src="<?php echo $featured_img_url; ?>" alt="Avatar" class="course_image">
		      <div class="text_course course_title"><b><?php echo get_the_title(); ?></b></div>

		  <div class="custom_course_overlay">
		    <div class="text_course"><a href="<?php echo $permalink; ?>"><u>Click to see lessons</u></a></div>
		  </div>
		</div>
	    <div class="custom_bottom_gap"></div>
	  <?php
	    /*echo '<li><a href='.$permalink.'>'.get_the_title().'</a></li>'; */
	endwhile;
echo '</div>';
	// Reset Post Data
	wp_reset_postdata();

	$args = array(
	    'post_type' => 'sfwd-courses',
	    'posts_per_page' => 4,
	    'tax_query' => array(
            array(
                'taxonomy' => 'ld_course_category',
                'field' => 'term_id',
                'terms' => $platinum_cat,
            )
        )
	    // Several more arguments could go here. Last one without a comma.F
	);

	// Query the posts:
	$obituary_query = new WP_Query($args);

	// Loop through the obituaries:
	echo '
	<div class="col-md-3 col-sm-6 platinum_sec common_course_Class">
		<h4 class="course_head">Platinum Certificate</h4>	
	';
	while ($obituary_query->have_posts()) : $obituary_query->the_post();
		$id = get_the_ID();
	    $permalink  = get_the_permalink();
	    $data = get_post_taxonomies( $post );
	    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
	    ?>
	   <div class="course_custom_container">
		  <img src="<?php echo $featured_img_url; ?>" alt="Avatar" class="course_image">
		      <div class="text_course course_title"><b><?php echo get_the_title(); ?></b></div>

		  <div class="custom_course_overlay">
		    <div class="text_course"><a href="<?php echo $permalink; ?>"><u>Click to see lessons</u></a></div>
		  </div>
		</div>
	    <div class="custom_bottom_gap"></div>
	  <?php
	    /*echo '<li><a href='.$permalink.'>'.get_the_title().'</a></li>'; */
	endwhile;
echo '</div>';
	// Reset Post Data
	wp_reset_postdata();

}
add_shortcode( 'course_table', 'courses_func' );

add_filter ('add_to_cart_redirect', 'redirect_to_checkout');

function redirect_to_checkout() {
       return WC()->cart->get_checkout_url();
}

/* end */
function myStartSession() {
    if(!session_id()) {
        session_start();
    }
}

function myEndSession() {
    session_destroy ();
}

add_theme_support( 'post-thumbnails' ); // this enable thumbnails
add_image_size( 'preview-thumb', 300, 185, true ); //this sets your default size

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );
function hero_data( $meta_boxes ) {
	$prefix = 'bizskills-';

	$meta_boxes[] = array(
		'id' => 'hero_data',
		'title' => esc_html__( 'hero banner data', 'hero_content' ),
		'post_types' => array( 'page', 'post' ),
		'context' => 'after_editor',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => $prefix . 'hero_title',
				'type' => 'text',
				'name' => esc_html__( 'hero title', 'hero_content' ),
			),
			array(
				'id' => $prefix . 'hero_content',
				'type' => 'textarea',
				'name' => esc_html__( 'hero content', 'hero_content' ),
			),
			array(
				'id' => $prefix . 'image_advanced_2',
				'type' => 'image_advanced',
				'name' => esc_html__( 'hero image', 'hero_content' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'hero_data' );
function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}

function dynamic_pricing_get()
{
	$country_code = ip_info("Visitor", "Country Code"); 
	
}
add_shortcode('dynamic_pricing', 'dynamic_pricing_get');

function HelloWorldShortcode() {
	$var =  '<div class="row latest_posts">';

	$the_query = new WP_Query('posts_per_page=3'); 
	if ( $the_query->have_posts() ) {
		// The Loop
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$title = get_the_title();
			$title = substr($title, 0, 25);
			$content = strip_tags(get_the_content());
			$content = substr($content, 0, 100);
			$date = get_the_date('d M, Y');
			$post_id = get_the_ID();
			/*$url =  get_the_post_thumbnail( $post_id, 'full' ); */
			$image = get_the_post_thumbnail_url(); 
			$image =  get_the_post_thumbnail_url( $post_id, array( 250, 250) );

			$link = get_the_permalink();
			$var .= '  <div class="col-md-4 col-sm-4">
			<div class="thumbnail latest_thumb">
			<a href="'.$link.'">
			<img src="'.$image.'">
			</a>
			<div class="caption">
			<h3 class="latest_grid_title">'.$title.'</h3>
			<p class="latest_grid_color">'.$content.'</p>
			<p class="latest_grid_title">'.$date.'</p>
			</div>
			</a>
			</div>
			</div>';
		}
		
		
		wp_reset_postdata();
	}
	$var .= '</div>';
	return $var;
}
add_shortcode('latest_news_grid', 'HelloWorldShortcode');


add_shortcode('course_pricing', 'popular_pricing');

function popular_pricing()
{


	$country_set3 = array(

			'AF'=>array('name'=>'AFGHANISTAN','code'=>'93'),
			'AO'=>array('name'=>'ANGOLA','code'=>'244'),
			'AM'=>array('name'=>'ARMENIA','code'=>'374'),
			'BD'=>array('name'=>'BANGLADESH','code'=>'880'),
			'BJ'=>array('name'=>'BENIN','code'=>'229'),
			'BT'=>array('name'=>'BHUTAN','code'=>'975'),
			'BO'=>array('name'=>'BOLIVIA','code'=>'591'),
			'BF'=>array('name'=>'BURKINA FASO','code'=>'226'),
			'BI'=>array('name'=>'BURUNDI','code'=>'257'),
			'CV'=>array('name'=>'CAPE VERDE','code'=>'238'),
			'KH'=>array('name'=>'CAMBODIA','code'=>'855'),
			'CM'=>array('name'=>'CAMEROON','code'=>'237'),
			'CF'=>array('name'=>'CENTRAL AFRICAN REPUBLIC','code'=>'236'),
			'TD'=>array('name'=>'CHAD','code'=>'235'),
			'KM'=>array('name'=>'COMOROS','code'=>'269'),
			'CD'=>array('name'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE','code'=>'243'),
			'CG'=>array('name'=>'CONGO','code'=>'242'),
			'CI'=>array('name'=>'COTE D IVOIRE','code'=>'225'),
			'DJ'=>array('name'=>'DJIBOUTI','code'=>'253'),
			'EG'=>array('name'=>'EGYPT','code'=>'20'),
			'SV'=>array('name'=>'EL SALVADOR','code'=>'503'),
			'ER'=>array('name'=>'ERITREA','code'=>'291'),
			'ET'=>array('name'=>'ETHIOPIA','code'=>'251'),
			'GM'=>array('name'=>'GAMBIA','code'=>'220'),
			'GE'=>array('name'=>'GEORGIA','code'=>'995'),
			'GH'=>array('name'=>'GHANA','code'=>'233'),
			'GT'=>array('name'=>'GUATEMALA','code'=>'502'),
			'GN'=>array('name'=>'GUINEA','code'=>'224'),
			'GW'=>array('name'=>'GUINEA-BISSAU','code'=>'245'),
			'HT'=>array('name'=>'HAITI','code'=>'509'),
			'HN'=>array('name'=>'HONDURAS','code'=>'504'),
			'IN'=>array('name'=>'INDIA','code'=>'91'),
			'ID'=>array('name'=>'INDONESIA','code'=>'62'),
			'JO'=>array('name'=>'JORDAN','code'=>'962'),
			'KE'=>array('name'=>'KENYA','code'=>'254'),
			'KI'=>array('name'=>'KIRIBATI','code'=>'686'),
			'KP'=>array('name'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF','code'=>'850'),
			'XK'=>array('name'=>'KOSOVO','code'=>'381'),
			'KG'=>array('name'=>'KYRGYZSTAN','code'=>'996'),
			'LA'=>array('name'=>'LAO PEOPLES DEMOCRATIC REPUBLIC','code'=>'856'),
			'LS'=>array('name'=>'LESOTHO','code'=>'266'),
			'LR'=>array('name'=>'LIBERIA','code'=>'231'),
			'MG'=>array('name'=>'MADAGASCAR','code'=>'261'),
			'MW'=>array('name'=>'MALAWI','code'=>'265'),
			'ML'=>array('name'=>'MALI','code'=>'223'),
			'MR'=>array('name'=>'MAURITANIA','code'=>'222'),
			'FM'=>array('name'=>'MICRONESIA, FEDERATED STATES OF','code'=>'691'),
			'MD'=>array('name'=>'MOLDOVA, REPUBLIC OF','code'=>'373'),
			'MN'=>array('name'=>'MONGOLIA','code'=>'976'),
			'MA'=>array('name'=>'MOROCCO','code'=>'212'),
			'MZ'=>array('name'=>'MOZAMBIQUE','code'=>'258'),
			'MM'=>array('name'=>'MYANMAR','code'=>'95'),
			'NP'=>array('name'=>'NEPAL','code'=>'977'),
			'NI'=>array('name'=>'NICARAGUA','code'=>'505'),
			'NE'=>array('name'=>'NIGER','code'=>'227'),
			'NG'=>array('name'=>'NIGERIA','code'=>'234'),
			'PK'=>array('name'=>'PAKISTAN','code'=>'92'),
			'PG'=>array('name'=>'PAPUA NEW GUINEA','code'=>'675'),
			'PH'=>array('name'=>'PHILIPPINES','code'=>'63'),
			'RW'=>array('name'=>'RWANDA','code'=>'250'),
			'ST'=>array('name'=>'SAO TOME AND PRINCIPE','code'=>'239'),
			'SN'=>array('name'=>'SENEGAL','code'=>'221'),
			'SL'=>array('name'=>'SIERRA LEONE','code'=>'232'),
			'SB'=>array('name'=>'SOLOMON ISLANDS','code'=>'677'),
			'SO'=>array('name'=>'SOMALIA','code'=>'252'),
			'SS'=>array('name'=>'South Sudan','code'=>''),
			'LK'=>array('name'=>'SRI LANKA','code'=>'94'),
			'SD'=>array('name'=>'SUDAN','code'=>'249'),
			'SZ'=>array('name'=>'SWAZILAND','code'=>'268'),
			'SY'=>array('name'=>'SYRIAN ARAB REPUBLIC','code'=>'963'),
			'TJ'=>array('name'=>'TAJIKISTAN','code'=>'992'),
			'TZ'=>array('name'=>'TANZANIA, UNITED REPUBLIC OF','code'=>'255'),
			'TL'=>array('name'=>'TIMOR-LESTE','code'=>'670'),
			'TG'=>array('name'=>'TOGO','code'=>'228'),
			'TN'=>array('name'=>'TUNISIA','code'=>'216'),
			'UG'=>array('name'=>'UGANDA','code'=>'256'),
			'UA'=>array('name'=>'UKRAINE','code'=>'380'),
			'UZ'=>array('name'=>'UZBEKISTAN','code'=>'998'),
			'VU'=>array('name'=>'VANUATU','code'=>'678'),
			'VN'=>array('name'=>'VIET NAM','code'=>'84'),
			'PS'=>array('name'=>'Palestinian Territory','code'=>''),
			'YE'=>array('name'=>'YEMEN','code'=>'967'),
			'ZM'=>array('name'=>'ZAMBIA','code'=>'260'),
			'ZW'=>array('name'=>'ZIMBABWE','code'=>'263'),
			'DZ'=>array('name'=>'ALGERIA','code'=>'213'),
			'BW'=>array('name'=>'BOTSWANA','code'=>'267'),
			'CO'=>array('name'=>'COLOMBIA','code'=>'57'),
			'CU'=>array('name'=>'CUBA','code'=>'53'),
			'GQ'=>array('name'=>'EQUATORIAL GUINEA','code'=>'240'),
			'GA'=>array('name'=>'GABON','code'=>'241'),
			'IQ'=>array('name'=>'IRAQ','code'=>'964'),
			'JM'=>array('name'=>'JAMAICA','code'=>'1876'),
			'LY'=>array('name'=>'LIBYAN ARAB JAMAHIRIYA','code'=>'218'),
			'MU'=>array('name'=>'MAURITIUS','code'=>'230'),
			'NA'=>array('name'=>'NAMIBIA','code'=>'264'),
			'ZA'=>array('name'=>'SOUTH AFRICA','code'=>'27'),
			'VE'=>array('name'=>'VENEZUELA','code'=>'58')
		);	
	$country_set2 = array(
			'AL'=>array('name'=>'ALBANIA','code'=>'355'),
			'AS'=>array('name'=>'AMERICAN SAMOA','code'=>'1684'),
			'AR'=>array('name'=>'ARGENTINA','code'=>'54'),
			'AR'=>array('name'=>'ARGENTINA','code'=>'54'),
			'AZ'=>array('name'=>'AZERBAIJAN','code'=>'994'),
			'BY'=>array('name'=>'Belarus','code'=>''),
			'BY'=>array('name'=>'Belarus','code'=>''),
			'BZ'=>array('name'=>'BELIZE','code'=>'501'),
			'BA'=>array('name'=>'BOSNIA AND HERZEGOVINA','code'=>'387'),
			'BR'=>array('name'=>'BRAZIL','code'=>'55'),
			'BG'=>array('name'=>'BULGARIA','code'=>'359'),
			'CN'=>array('name'=>'CHINA','code'=>'86'),
			'CR'=>array('name'=>'COSTA RICA','code'=>'506'),
			'HR'=>array('name'=>'CROATIA','code'=>'385'),
			'DM'=>array('name'=>'DOMINICA','code'=>'1767'),

			'DO'=>array('name'=>'DOMINICAN REPUBLIC','code'=>'1809'),
			'EC'=>array('name'=>'ECUADOR','code'=>'593'),
			'FJ'=>array('name'=>'FIJI','code'=>'679'),
			'GD'=>array('name'=>'GRENADA','code'=>'1473'),
			'GY'=>array('name'=>'GUYANA','code'=>'592'),
			'IR'=>array('name'=>'IRAN, ISLAMIC REPUBLIC OF','code'=>'98'),
			'KZ'=>array('name'=>'Kazakhstan','code'=>''),
			'LB'=>array('name'=>'LEBANON','code'=>'961'),

			'MK'=>array('name'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','code'=>'389'),
			'MY'=>array('name'=>'MALAYSIA','code'=>'60'),
			'MV'=>array('name'=>'MALDIVES','code'=>'960'),
			'MH'=>array('name'=>'MARSHALL ISLANDS','code'=>'692'),
			'MX'=>array('name'=>'MEXICO','code'=>'52'),
			'ME'=>array('name'=>'MONTENEGRO','code'=>'382'),
			'NR'=>array('name'=>'NAURU','code'=>'674'),
			'PA'=>array('name'=>'PANAMA','code'=>'507'),
			'PY'=>array('name'=>'PARAGUAY','code'=>'595'),
			'PE'=>array('name'=>'PERU','code'=>'51'),
			'RO'=>array('name'=>'ROMANIA','code'=>'40'),
			'RU'=>array('name'=>'RUSSIAN FEDERATION','code'=>'7'),
			'WS'=>array('name'=>'SAMOA','code'=>'685'),
			'RS'=>array('name'=>'SERBIA','code'=>'381'),
			'LC'=>array('name'=>'SAINT LUCIA','code'=>'1758'),
			'VC'=>array('name'=>'SAINT VINCENT AND THE GRENADINES','code'=>'1784'),
			'SR'=>array('name'=>'SURINAME','code'=>'597'),
			'TH'=>array('name'=>'THAILAND','code'=>'66'),
			'TO'=>array('name'=>'TONGA','code'=>'676'),
			'TR'=>array('name'=>'TURKEY','code'=>'90'),
			'TM'=>array('name'=>'TURKMENISTAN','code'=>'993'),
			'TV'=>array('name'=>'TUVALU','code'=>'688')
		);

	$country_set1 = array(
		'AD'=>array('name'=>'ANDORRA','code'=>'376'),
		'AG'=>array('name'=>'ANTIGUA AND BARBUDA','code'=>'1268'),
		'AW'=>array('name'=>'ARUBA','code'=>'297'),
		'AU'=>array('name'=>'AUSTRALIA','code'=>'61'),
		'AT'=>array('name'=>'AUSTRIA','code'=>'43'),
		'BS'=>array('name'=>'BAHAMAS','code'=>'1242'),
		'BH'=>array('name'=>'BAHRAIN','code'=>'973'),
		'BB'=>array('name'=>'BARBADOS','code'=>'1246'),
		'BE'=>array('name'=>'BELGIUM','code'=>'32'),
		'BM'=>array('name'=>'BERMUDA','code'=>'1441'),
		'VG'=>array('name'=>'British Virgin Islands','code'=>''),
		'BN'=>array('name'=>'BRUNEI DARUSSALAM','code'=>'673'),
		'CA'=>array('name'=>'CANADA','code'=>'1'),
		'KY'=>array('name'=>'CAYMAN ISLANDS','code'=>'1345'),
		'empty'=>array('name'=>'Channel Islands','code'=>'1345'),
		'CL'=>array('name'=>'CHILE','code'=>'56'),
		'CW'=>array('name'=>'Curaçao','code'=>''),
		'CY'=>array('name'=>'CYPRUS','code'=>'357'),
		'CZ'=>array('name'=>'CZECH REPUBLIC','code'=>'420'),
		'DK'=>array('name'=>'DENMARK','code'=>'45'),
		'EE'=>array('name'=>'ESTONIA','code'=>'372'),
		'FO'=>array('name'=>'FAROE ISLANDS','code'=>'298'),
		'FI'=>array('name'=>'FINLAND','code'=>'358'),
		'FR'=>array('name'=>'FRANCE','code'=>'33'),
		'PF'=>array('name'=>'FRENCH POLYNESIA','code'=>'689'),
		'DE'=>array('name'=>'GERMANY','code'=>'49'),
		'GI'=>array('name'=>'GIBRALTAR','code'=>'350'),
		'GR'=>array('name'=>'GREECE','code'=>'30'),
		'GL'=>array('name'=>'GREENLAND','code'=>'299'),
		'GU'=>array('name'=>'GUAM','code'=>'1671'),
		'HK'=>array('name'=>'Hong Kong SAR, China','code'=>'852'),
		'HU'=>array('name'=>'HUNGARY','code'=>'36'),
		'IS'=>array('name'=>'ICELAND','code'=>'354'),
		'IE'=>array('name'=>'IRELAND','code'=>'353'),
		'IM'=>array('name'=>'ISLE OF MAN','code'=>'44'),
		'IL'=>array('name'=>'ISRAEL','code'=>'972'),
		'IT'=>array('name'=>'ITALY','code'=>'39'),
		'JP'=>array('name'=>'JAPAN','code'=>'81'),
		'KR'=>array('name'=>'Korea, Rep.','code'=>''),
		'KW'=>array('name'=>'KUWAIT','code'=>'965'),
		'LV'=>array('name'=>'LATVIA','code'=>'371'),
		'LI'=>array('name'=>'LIECHTENSTEIN','code'=>'423'),
		'LT'=>array('name'=>'LITHUANIA','code'=>'370'),
		'LU'=>array('name'=>'LUXEMBOURG','code'=>'352'),
		'MO'=>array('name'=>'Macao SAR, China','code'=>'853'),
		'MT'=>array('name'=>'MALTA','code'=>'356'),
		'MC'=>array('name'=>'MONACO','code'=>'377'),
		'AN'=>array('name'=>'NETHERLANDS ANTILLES','code'=>'599'),
		'NC'=>array('name'=>'NEW CALEDONIA','code'=>'687'),
		'NZ'=>array('name'=>'NEW ZEALAND','code'=>'64'),
		'MP'=>array('name'=>'NORTHERN MARIANA ISLANDS','code'=>'1670'),
		'NO'=>array('name'=>'NORWAY','code'=>'47'),
		'OM'=>array('name'=>'OMAN','code'=>'968'),
		'PW'=>array('name'=>'PALAU','code'=>'680'),
		'PL'=>array('name'=>'POLAND','code'=>'48'),
		'PT'=>array('name'=>'PORTUGAL','code'=>'351'),
		'PR'=>array('name'=>'PUERTO RICO','code'=>'1'),
		'QA'=>array('name'=>'QATAR','code'=>'974'),
		'SM'=>array('name'=>'SAN MARINO','code'=>'378'),
		'SA'=>array('name'=>'SAUDI ARABIA','code'=>'966'),
		'SC'=>array('name'=>'SEYCHELLES','code'=>'248'),
		'SG'=>array('name'=>'SINGAPORE','code'=>'65'),
		'SX'=>array('name'=>'Sint Maarten (Dutch part)','code'=>''),
		'SK'=>array('name'=>'SLOVAKIA','code'=>'421'),
		'SI'=>array('name'=>'SLOVENIA','code'=>'386'),
		'ES'=>array('name'=>'SPAIN','code'=>'34'),
		'KN'=>array('name'=>'SAINT KITTS AND NEVIS','code'=>'1869'),
		'MF'=>array('name'=>'Saint-Martin (French part)','code'=>''),
		'SE'=>array('name'=>'SWEDEN','code'=>'46'),
		'CH'=>array('name'=>'SWITZERLAND','code'=>'41'),
		'TW'=>array('name'=>'TAIWAN, PROVINCE OF CHINA','code'=>'886'),
		'TT'=>array('name'=>'TRINIDAD AND TOBAGO','code'=>'1868'),
		'TC'=>array('name'=>'TURKS AND CAICOS ISLANDS','code'=>'1649'),
		'AE'=>array('name'=>'UNITED ARAB EMIRATES','code'=>'971'),
		'GB'=>array('name'=>'UNITED KINGDOM','code'=>'44'),
		'US'=>array('name'=>'UNITED STATES','code'=>'1'),
		'UY'=>array('name'=>'URUGUAY','code'=>'598'),
		'US'=>array('name'=>'UNITED STATES','code'=>'1'),
		'VI'=>array('name'=>'VIRGIN ISLANDS, U.S.','code'=>'1340')
		);
  


  $country_code = strtoupper(ip_info("Visitor", "Country Code")); 

 
  
  $early_stage_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2612";
  $educational_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2960";
  $business_advisory_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2965";
  $govt_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2971";

  if (array_key_exists($country_code,$country_set3))
  {
  	
	$early_stage_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2958";
  	$educational_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2961";
  	$business_advisory_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2966";
  	$govt_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2972";

  }
  if (array_key_exists($country_code,$country_set2))
  {
  	$early_stage_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2612";
  	$educational_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2960";
  	$business_advisory_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2965";
  	$govt_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2971";
  	
  }
   if (array_key_exists($country_code,$country_set1))
  {

  	$early_stage_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2956";
  	$educational_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2959";
  	$business_advisory_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2964";
  	$govt_link = "https://bizskillsacademy.com/bizcustom/?add-to-cart=2970";
  }

	$var = '
	<div class="row">
	<div class="col-md-3 col-sm-6">
		 <div class="columns">
			  <ul class="price">
			    <li class="header header_pricing">Early-Stage Entrepreneurs</li>
			    <li class="sub_header_pricing">3 Months<br> Plan</li>
			    <li><img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/tick-icon.png" class="item_icon">&nbsp;&nbsp;&nbsp;<font class="item_text">An﻿ extended benefit</font></li>
			     <li><img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/tick-icon.png" class="item_icon">&nbsp;&nbsp;&nbsp;<font class="item_text">An﻿ extended benefit</font></li>
			    <li class="button_pricing_li" ><a href="'.$early_stage_link.'" class="button button_pricing">Buy Now</a></li>
			   
			  </ul>
		</div> 
	</div>
	
	<div class="col-md-3 col-sm-6">
		<div class="columns">
			  <ul class="price">
			    <li class="header header_pricing">Educational Institutions</li>
			    <li class="sub_header_pricing">Collaboration & <br> Facilitation</li>
			    <li><img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/tick-icon.png" class="item_icon">&nbsp;&nbsp;&nbsp;<font class="item_text">An﻿ extended benefit</font></li>
			     <li><img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/tick-icon.png" class="item_icon">&nbsp;&nbsp;&nbsp;<font class="item_text">An﻿ extended benefit</font></li>
			    <li class="button_pricing_li" ><a href="'.$educational_link.'" class="button button_pricing">Buy Now</a></li>
			    
			  </ul>
		</div> 
	</div>
	<div class="col-md-3 col-sm-6">
		  <div class="columns">
			  <ul class="price">
			    <li class="header header_pricing">Business <br> Advisory </li>
			    <li class="sub_header_pricing">1 Super-user Account + 15 Sub-users</li>
			    <li><img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/tick-icon.png" class="item_icon">&nbsp;&nbsp;&nbsp;<font class="item_text">An﻿ extended benefit</font></li>
			     <li><img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/tick-icon.png" class="item_icon">&nbsp;&nbsp;&nbsp;<font class="item_text">An﻿ extended benefit</font></li>
			    <li class="button_pricing_li" ><a href="'.$business_advisory_link.'" class="button button_pricing">Buy Now</a></li>
			  </ul>
			</div>  
	</div>
	<div class="col-md-3 col-sm-6">
		  <div class="columns">
			  <ul class="price">
			    <li class="header header_pricing">Government and NGOs</li>
			    <li class="sub_header_pricing">Policy Harmonization & LMS integration </li>
			    <li><img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/tick-icon.png" class="item_icon">&nbsp;&nbsp;&nbsp;<font class="item_text">An﻿ extended benefit</font></li>
			     <li><img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/tick-icon.png" class="item_icon">&nbsp;&nbsp;&nbsp;<font class="item_text">An﻿ extended benefit</font></li>
			    <li class="button_pricing_li"><a href="'.$govt_link	.'" class="button button_pricing">Buy Now</a></li>
			  </ul>
			</div> 
	</div>
</div>

	';
	return $var;
}

function social_media_call() {
	$var =  '<ul class="footer_list custom_social_sec">';

	$var .='<li>
	<a class="social_li" href="https://www.linkedin.com/company/bizsqills?trk=biz-companies-cym">
		<img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/04/linkedin-1.png">
	</a>
	<a class="social_li"  href="https://www.facebook.com/BizSkillsAcademy/?ref=aymt_homepage_panel">
		<img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/04/facebook-1.png">
	</a> 
	<a class="social_li" href="https://www.instagram.com/bizskills_academy_official/?hl=en">
		<img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/04/instagram-1.png">
	</a>
</li>
<li>
	<a class="social_li" href="https://plus.google.com/111821056062242425721">
		<img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/04/google-plus.png">
	</a>
	<a class="social_li" href="https://twitter.com/bizskillsacadmy">
		<img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/04/twitter-1.png">
	</a> 
	<a class="social_li" href="https://www.youtube.com/channel/UC1Yk35F4z6gEZPNivjkb6Kw">
		<img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/04/youtube-1.png">
	</a>
</li>
	';
	$var .= '</ul>';
	$var = trim($var);
	return $var;
}
add_shortcode('social_media', 'social_media_call');



function testimonial_fetch() {
	
	$var =  '<div class="container">
	<div class="row">
	    <div class="col-md-12" data-wow-delay="0.2s">
	        <div class="carousel slide" data-ride="carousel" id="quote-carousel" data-interval="300000">';

		$the_query = new WP_Query( array( 'post_type' => 'biz_testimonial' ) );
		$posts = $the_query->posts;
		$var .= '<ol class="carousel-indicators">';
		$count = 1;

		$slide_number = 1;
		$c = 0;
		foreach($posts as $data)
		{
			$active = "";
			$post_id = $data->ID;
			
			$url = wp_get_attachment_url( get_post_thumbnail_id($post_id) );

		 	$url = get_the_post_thumbnail_url( $post_id, array( 115, 115) );
			
			if($count == 1)
			{
				$active = "active";
			}
			if($count > 1)
			{
				if($count%4 == 1)
			    {
			    	$slide_number++;
			    }
			}

			  
			$var .= '<li data-target="#quote-carousel" data-slide-to="'.$count.'" class="'.$active.' hide slide'.$slide_number.' slide_li"><img class="img-responsive " src="'.$url.'" alt=""></li>';
			    $count++;  
			    $c++;       
		}
		$var .= '</ol>';
		$var .='<div class="row text-center" style="visibility: hidden; opacity: 0;height: 20px;">
		<h2 class="quote_start"><img src="https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/arrow_icon_testk.png"/></h2></div>';



		$num = 1;
		$var .= '<div class="carousel-inner text-center">';
		foreach($posts as $data)
		{
			$post_id = $data->ID;
			$content = trim(strip_tags($data->post_content,'<iframe>'));
			$active = "";
			$author_name = get_post_meta(1589,'_bt_author_name');
			
			$meta = get_post_meta($post_id, '', true);
			
			$author_name = "";
			$author_website = "";

			if(isset($meta['_bt_author_name']['0']))
			{
				
				$author_name =trim($meta['_bt_author_name']['0']);
			}


			
			if(isset($meta['_bt_author_website']['0']))
			{
				$author_website =trim($meta['_bt_author_website']['0']);
				$author_website = preg_replace('#^https?://#', '', $author_website);
				$author_website = rtrim($author_website,"/");
			}
			

			if($num == 1)
			{
				$active = "active";
			}
				
			$var .= '<div class="item '.$active.'">                    
		                <div class="row">
		                    <div class="col-sm-10 custom_col">

		                        <p>'.$content.'</p>
		                        <h3 class="author_name_testimonial">'.$author_name.'</h3>
		                        <p class="author_website_testimonial">'.$author_website.'</p>
		                    </div>
		                </div>
		            </div>'; 
		    $num++;         
		}

		$var .= '</div>

		<a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
	   	<a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>';

	$var .='</div>
	   		</div>
		</div>
	</div>';	
	wp_reset_postdata();

	return $var;
}

add_shortcode('testimonial_data', 'testimonial_fetch');

function get_excerpt() {
	$excerpt = get_the_content();
	$excerpt = preg_replace(" ([.*?])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 50);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));
	$excerpt = $excerpt.'... <a href="'.$permalink.'">more</a>';
	return $excerpt;
}

add_filter('gettext', 'change_howdy', 10, 3);
function change_howdy($translated, $text, $domain) {

    if (!is_admin() || 'default' != $domain)
        return str_replace('Howdy', 'Welcome', $translated);

    if (false !== strpos($translated, 'Howdy'))
        return str_replace('Howdy', 'Welcome', $translated);

    return $translated;
}

ini_set("include_path", '/home/musengimana/php:' . ini_get("include_path") );

function willow_child_theme_enqueue_scripts() {
	wp_register_style( 'mediastyle', get_stylesheet_directory_uri() . '/css/media.css', array("style"), "0007"  );
	wp_enqueue_style( 'mediastyle' );
}
add_action('wp_enqueue_scripts', 'willow_child_theme_enqueue_scripts', 11);

global $lms_wpdb;
$lms_wpdb = new wpdb( 'lms_softwere', 'pass125', 'lms_softwere', 'localhost');
$lms_wpdb->show_errors();

function register_my_menus() {
  register_nav_menus (
    array (
      'inner_menu' => __( 'Inner Page Menu' )     
    )
  );
}
add_action( 'init', 'register_my_menus' );

function new_contact_methods( $contactmethods ) {
    $contactmethods['lms_login'] = 'Lms Login';
    return $contactmethods;
}
add_filter( 'user_contactmethods', 'new_contact_methods', 10, 1 );

function new_modify_user_table( $column ) {
    $column['lms_login'] = 'Lms Login';
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'lms_login':    

			$lms_currency = get_user_meta($user_id, 'lms-currency', true);
			$lms_business_name = get_user_meta($user_id, 'lms-business-name', true);
			$lms_business_month = get_user_meta($user_id, 'lms-business-month', true);
			$lms_business_year = get_user_meta($user_id, 'lms-business-year', true);
			//$lms_business_baseyear = get_user_meta($user_id, 'lms-business-baseyear', true);
			$lms_business_projectid = get_user_meta($user_id, 'lms-business-projectid', true);
			$lms_business_taxpercent = get_user_meta($user_id, 'lms-business-taxpercent', true);
			$lms_business_userrole = get_user_meta($user_id, 'lms-business-userrole', true);
			$userdata = get_userdata( $user_id );

	        $user_img_arr = get_user_meta($user_id, "basic_user_avatar");
		    if(empty($user_img_arr)) {
		    	$user_img_arr[0]["full"] = 'https://bizskillsacademy.com/wp-content/uploads/2016/06/apple-icon-60x60.png';
		    }	

			if( !empty($lms_currency) && 
				!empty($lms_business_name) && 
				!empty($lms_business_month) && 
				!empty($lms_business_year) && 				
				!empty($lms_business_projectid) && 
				!empty($lms_business_taxpercent) && 
				!empty($lms_business_userrole) ) {

            	return '<p class="submit"><a id="uid_'.$user_id.'" 
            		data-email="'.$userdata->user_email.'" 
            		data-username="'.$userdata->user_login.'" 
            		data-pass="'.$userdata->user_pass.'" 
            		data-userid="'.$user_id.'" 
            		data-curreney="'.$lms_currency.'" 
            		data-bname="'.$lms_business_name.'" 
            		data-bmonth="'.$lms_business_month.'" 
            		data-byear="'.$lms_business_year.'"  
            		data-projectid="'.$lms_business_projectid.'" 
            		data-taxpercent="'.$lms_business_taxpercent.'" 
            		data-userRole="'.$lms_business_userrole.'"  
            		data-profile_pic="'.$user_img_arr[0]['full'].'"
            		class="lmslogin button button-primary">Lms Login</a></p>';
	    	} else {
	    		return '<p class="submit"><a data-username="'.$userdata->user_nicename.'" id="uid_'.$user_id.'" class="lmslogin button button-primary">Lms Login</a>
	    		</p>';
	    	}
            break;
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );

add_action( 'admin_footer', 'my_admin_footer_function' );
function my_admin_footer_function() {
	echo '<script type="text/javascript">
		(function($) {
			$( document ).on( "click", ".lmslogin", function(evt) {
				if($(this).data("email")){				
					$("#hdn_user_email").val($(this).data("email"));
					$("#hdn_user_name").val($(this).data("username"));
					$("#hdn_user_pass").val($(this).data("pass"));
					$("#hdn_user_id").val($(this).data("userid"));
					$("#hdn_lms_cur").val($(this).data("curreney"));
					$("#hdn_lms_bname").val($(this).data("bname"));
					$("#hdn_lms_bmonth").val($(this).data("bmonth"));
					$("#hdn_lms_byear").val($(this).data("byear"));					
					$("#hdn_lms_projectid").val($(this).data("projectid"));
					$("#hdn_lms_taxpercent").val($(this).data("taxpercent"));
					$("#hdn_lms_userrole").val($(this).data("userRole"));
					$("#hdn_profile_pic").val($(this).data("profile_pic"));
					setTimeout(function(){ 						
						$("#lms_submit").trigger( "click" ) }, 1000);		
				} else {
					alert($(this).data("username")+" is not fill the lms data yet.");
				}
				evt.preventDefault();
			});
		})(jQuery);
	</script>
	<form style="display: none; position: absolute;" class="login-wrapper" id="login_form" 
	action="https://lms.bizskillsacademy.com/site/wplogin" target="_blank" method="post">
		<input type="hidden" id="hdn_profile_pic" name="hdn_profile_pic" value="" /> 
	    <input type="hidden" id="hdn_user_email" name="hdn_user_email" value="" /> 
	    <input type="hidden" id="hdn_user_name" name="username" value="" /> 
	    <input type="hidden" id="hdn_user_pass" name="password" value="" /> 
	    <input type="hidden" id="hdn_user_id" name="hdn_user_id" value="" /> 
	    <input type="hidden" id="hdn_lms_cur" name="hdn_lms_data[currency]" value=""> 
	    <input type="hidden" id="hdn_lms_bname" name="hdn_lms_data[business_name]" value=""> 
	    <input type="hidden" id="hdn_lms_bmonth" name="hdn_lms_data[business_month]" value=""> 	   
	    <input type="hidden" id="hdn_lms_byear" name="hdn_lms_data[business_year]" value=""> 
		<input type="hidden" id="hdn_lms_projectid" name="hdn_lms_data[business_projectid]" value="">
		<input type="hidden" id="hdn_lms_taxpercent" name="hdn_lms_data[business_taxpercent]" value="">
		<input type="hidden" id="hdn_lms_userrole" name="hdn_lms_data[business_userrole]" value="">
		<p class="submit"><input type="submit" name="submit" id="lms_submit" class="button button-primary" value="Lms Login"></p></form>';
}

function my_login_redirect( $redirect_to, $request, $user ) { 	
	//is there a user to check?

	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place					
			return $redirect_to;
		//editor
		} else if ( in_array( 'editor', $user->roles ) ) {
			wp_redirect( home_url() .'/wp-admin' );
			exit();
		} else if ( in_array( 'author', $user->roles ) ) {
			// redirect them to the default place					
			//return $redirect_to;
			wp_redirect( home_url() .'/wp-admin' );
			exit();
		} else if ( in_array( 'coach', $user->roles ) ) {
			wp_redirect( esc_url( get_permalink(3259) ) );
			exit();
		}else {
			//subscriber
			if ( in_array( 'subscriber', $user->roles ) ) {
				$user_id = $user->ID;
				user_default_values();
			 	$lms_currency = get_user_meta($user_id, 'lms-currency', true);
			 	$lms_business_name = get_user_meta($user_id, 'lms-business-name', true);
			 	$lms_business_month = get_user_meta($user_id, 'lms-business-month', true);
			 	$lms_business_year = get_user_meta($user_id, 'lms-business-year', true);
			 	//$lms_business_baseyear = get_user_meta($user_id, 'lms-business-baseyear', true);
			 	
			 	if(!empty($lms_currency) && !empty($lms_business_name) && !empty($lms_business_month) && !empty($lms_business_year) ) {
			 		if($_REQUEST['redirect_to'] == 'https://bizskillsacademy.com/lms-data/'){
			 			wp_redirect( esc_url( get_permalink(1534) ) );
			 		} else {
						wp_redirect( esc_url( get_permalink(19) ) );
					}			 		
					exit();
			 	} else {
			 		
			 		wp_redirect( esc_url( get_permalink(1534) ) );
					exit();
			 	}
		 	} else {
		 		return $redirect_to;
		 	}
		}
	} else {		
		return $redirect_to;
	}
}
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

global $current_user;

function add_html_to_level(){
   	echo '<h3 class="topborder">Hours Subscription</h3>';
   	echo '<table class="form-table">
			<tbody>
				<tr>
				<th valign="top" scope="row"><label>Courses:</label></th>
				<td>
					<ul>
					<li>
						<input class="hours" type="checkbox" value="5" name="hour" id="hour"> 
						Five Hours
					</li>
					<li>
						<input class="hours" type="checkbox" value="8" name="hour" id="hour"> 
						Eight Hours
					</li>
					</ul>
				</td>
				</tr>
			</tbody>
		</table>';
	echo "<script type='text/javascript'>
		(function($) {
			$('.hours').on('change', function() {
			   	$('.hours').not(this).prop('checked', false);
			   	if($(this).prop('checked') == false) {
    				var i = window.confirm('Sure?'');
			        if(i == true) {
			        	console.log('test');
			        } else {
			        	console.log('else');
			        }	
    			} else {
    			}
			}); 
		})(jQuery);
	</script>";
}

//add_action('pmpro_membership_level_after_other_settings', add_html_to_level);

add_filter( 'wp_mail_from', 'custom_wp_mail_from' );
function custom_wp_mail_from( $original_email_address ) {
	//Make sure the email is from the same domain 
	//as your website to avoid being marked as spam.
	return 'support@bizskillsacademy.com';
}

add_filter( 'wp_mail_from_name', 'custom_wp_mail_from_name' );
function custom_wp_mail_from_name( $original_email_from ) {
	return 'Bizskills Academy';
}

add_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );
function wpdocs_set_html_mail_content_type() {
    return 'text/html';
}

add_action('wp_head','hook_css');
function hook_css() {
	if ( is_user_logged_in() ) {
		$output='<style type="text/css">a[href*="https://bizskillsacademy.com/levels/"]{ display:none; }</style>';		
		echo $output;
	}
}

add_action( 'wp_footer', 'hook_footer_script' );
function hook_footer_script() {
	global $wpdb;
	global $lms_wpdb;
	global $current_user;
	$user_id = $current_user->ID;
	$user_meta = get_userdata($user_id); 
	$user_roles = $user_meta->roles;  
	$output = "";
	if (in_array("subscriber", $user_roles) || pmpro_hasMembershipLevel()) {
		$lms_currency = get_user_meta($user_id, 'lms-currency', true);
		$lms_business_name = get_user_meta($user_id, 'lms-business-name', true);
		$lms_business_month = get_user_meta($user_id, 'lms-business-month', true);
		$lms_business_year = get_user_meta($user_id, 'lms-business-year', true);
		//$lms_business_baseyear = get_user_meta($user_id, 'lms-business-baseyear', true);
		$lms_business_projectid = get_user_meta($user_id, 'lms-business-projectid', true);
		$lms_business_taxpercent = get_user_meta($user_id, 'lms-business-taxpercent', true);
		$lms_business_userrole = get_user_meta($user_id, 'lms-business-userrole', true);

		if( !empty($lms_currency) 
			&& !empty($lms_business_name) 
			&& !empty($lms_business_month) 
			&& !empty($lms_business_year) 
			&& !empty($lms_business_projectid) 
			&& !empty($lms_business_taxpercent) 
			&& !empty($lms_business_userrole) ) 
		{
		    $userdata = get_userdata( $user_id );
		    $user_img_arr = get_user_meta($user_id, "basic_user_avatar");
	   
		    if(empty($user_img_arr)){
		      $user_img_arr[0]["full"] = 'https://1.gravatar.com/avatar/7eddb39c5e3dd4ea291558c62b2b2b2c?s=96&d=https%3A%2F%2Fbizskillsacademy.com%2Fwp-content%2Fuploads%2F2016%2F06%2Fapple-icon-152x152.png&r=g';
		    }
			$output .= '<form id="login_form_footer" action="https://lms.bizskillsacademy.com/site/wplogin" method="post">
				<input type="hidden" name="hdn_profile_pic" value="'.$user_img_arr[0]['full'].'" /> 
				<input type="hidden" name="hdn_user_email" value="'.$userdata->user_email.'" /> 
				<input type="hidden" name="username" value="'.$userdata->user_login.'" />
				<input type="hidden" name="password" value="'.$userdata->user_pass.'" />
				<input type="hidden" name="hdn_user_id" value="'.$userdata->ID.'" /> 
				<input type="hidden" name="hdn_lms_data[currency]" value="'.$lms_currency.'"> 
				<input type="hidden" name="hdn_lms_data[business_name]" value="'.$lms_business_name.'"> 
				<input type="hidden" name="hdn_lms_data[business_month]" value="'.$lms_business_month.'"> 
			    <input type="hidden" name="hdn_lms_data[business_year]" value="'.$lms_business_year.'">
				<input type="hidden" id="hdn_business_pro_id_multi" name="hdn_lms_data[business_projectid]" value="'.$lms_business_projectid.'"> 
				<input type="hidden" name="hdn_lms_data[business_taxpercent]" value="'.$lms_business_taxpercent.'"> 
				<input type="hidden" name="hdn_lms_data[business_userrole]" value="'.$lms_business_userrole.'">     
				</form>';		 
			$output .= "<script type='text/javascript'>
				(function($) {
					$( '.fbps_lms_login' ).click(function() {		
						$(\"#lms-projects\").modal({
						  fadeDuration: 100
						});
						//$('#login_form_footer').submit(); 
					});
					$( '#subscriber-lms-btns' ).fadeIn('slow');
				})(jQuery);
			</script>";   
		} else {
			$output .= "<script type='text/javascript'>
				(function($) {
					$( '.fbps_lms_login' ).click(function() {				  
						alert('Please fill the Business Data first.');
						location.assign('".get_permalink(1534)."'); 
					});			
					$( '#subscriber-lms-btns' ).fadeIn('slow');
				})(jQuery);

			</script>";   
		}
	}
	$output .= "<script type='text/javascript'>
			(function($) {
				$('.feature:nth-last-child(2)').toggle(function(){
					if($(window).width()>320){
						//$(this).animate({'height': '120px'}, 200);
					} else {
						//$(this).animate({'height': '95px'}, 200);
					}
				}, function(){
					if($(window).width()>320){
						//$(this).animate({'height': '50px'}, 200);
					} else {
						//$(this).animate({'height': '25px'}, 200);
					}				    
				});
				$('.feature:nth-last-child(2)').mouseover(function(){
					//$(this).animate({'height': '120px'}, 200);
				}).mouseout(function(){
					//$(this).animate({'height': '50px'}, 200);
				})
				if($('.bt-text iframe').length){
					//console.log($('.bt-text iframe').parents('.bt-text'));
					$('iframe').parents('.bt-text').css( 'height', 'auto' );
				}
				var leasingsoftwareinfo = '1';
				$( '.col-md-2:last-child ul li.pricing-footer.footer-row > a.btn-pricing.btn.btn-success.btn-block' ).click(function(e) {					
					if(leasingsoftwareinfo=='1'){	
						console.log(leasingsoftwareinfo);	
						$('.wpb_text_column.wpb_content_element.leasing-software-info').fadeIn('slow');
						leasingsoftwareinfo = '0';

					}
					//e.preventDefault();
				});

				$( document ).delegate( '#btn_multiple_project', 'click', function(e) {
  					
  					if($('#dlst_business_project_mak').val()!=0){
  						if($('#hdn_business_pro_id_multi').val()!=''){
  							$('#login_form_footer').submit();
  						}
  					} else {
  						$('#dlst_business_project_mak').css('outline','1px solid red');
  					}
  					e.preventDefault();
				});

				$( document ).delegate( '#dlst_business_project_mak', 'change', function(e) {
					$('#dlst_business_project_mak').delay(5000).css('outline','none');
					if($(this).val()!=0){
						$('#hdn_business_pro_id_multi').val($(this).val());	
					} else {		
						$('#hdn_business_pro_id_multi').val('');				
					} 		
					e.preventDefault();			
				});

			})(jQuery);
		</script>";  

	$output .= '<div id="lms-projects" class="modall" style="display: none;">';
	$project_arr = $lms_wpdb->get_results( 'SELECT id, user_id, name, currency FROM tbl_project where user_id = '.$user_id, OBJECT );  

	$output .= '<table border="0" cellspacing="0" cellpadding="0">';
	$output .= '<tr><td calspan="2"><h4 class="financial_planning">Financial Planning</h4></td></tr>'; 
    $output .= '<tr>';

    if(!empty($project_arr)) {
    	$output .= '<td><select id="dlst_business_project_mak" name="dlst_business_project_mak">
          <option value="0">Select Project</option>';
        foreach ( $project_arr as $project_row ) 
        {
        	$output .= '<option value="'.$project_row->id.'">'.$project_row->name.'</option>';
		}   
        $output .= '</select></td>';
    	$output .= '<td><a href="javascript:void(0);" id="btn_multiple_project">Submit</a></td>';    	
    } else {
    	$output .= '<td>Please fill the Business Data first.Click submit</td>';
    	$output .= '<td><a href="'.get_permalink(1534).'">Submit</a></td>';
    }

	$output .= '</tr>';
   	$output .= '</table>';
	$output .= '</div>';

	echo $output;
}

function func_user_geoip_city(){
	require_once "../../php/Net/GeoIP.php";
	$geoip = Net_GeoIP::getInstance( "../../php/Net/GeoLiteCity.dat", Net_GeoIP::SHARED_MEMORY );

	try {
	    $location = $geoip->lookupLocation($_SERVER['REMOTE_ADDR']);
	    //var_dump($location);
	   	return 'in '.$location->city;
	} catch (Exception $e) {
		return ' ';
	   // print_r($e);
	}
}
add_shortcode( 'user_geoip_city', 'func_user_geoip_city' );

if(isset($_SERVER['HTTP_REFERER'])) {
	
	$refData = parse_url($_SERVER['HTTP_REFERER']);
	if($refData["host"] == "lms.bizskillsacademy.com"){
		$_SESSION['lms_referer'] = $_SERVER['HTTP_REFERER'];		
	} else {
		//unset($_SESSION['lms_referer']);
	}	
}

add_action( 'wp_loaded','user_default_values' );
function user_default_values() {
	global $lms_wpdb;
	global $wpdb;
	global $current_user;

	if ( is_user_logged_in() )
  	{	
  		$user_id = $current_user->ID;
  		if ( ! is_admin() ) {
  			$project_arr = $lms_wpdb->get_results( 'SELECT id, user_id, name, currency FROM tbl_project where user_id = '.$user_id.' LIMIT 1', OBJECT );  
            if(!empty($project_arr)) {

				$unforeseen_percentage_arr = $lms_wpdb->get_results( 'SELECT * FROM tbl_unforeseen where user_id = '.$user_id.' LIMIT 1', OBJECT ); 
				if(empty($unforeseen_percentage_arr)) 
				{
				 	$ins_project = $lms_wpdb->insert ( 
		                'tbl_unforeseen', 
		                array ( 
		                  'project_id' => $project_arr[0]->id, 
		                  'user_id' => $user_id,
		                  'percent' => 1
		                ), 
		                array ( 
		                  '%d', 
		                  '%d',
		                  '%d'
		                ) 
		            );
				}
				$working_capital_months_arr = $lms_wpdb->get_results( 'SELECT * FROM tbl_capitalmonths where user_id = '.$user_id.' AND item = "working capital" LIMIT 1', OBJECT ); 
				if(empty($unforeseen_percentage_arr))
				{
					$ins_project = $lms_wpdb->insert ( 
		                'tbl_capitalmonths', 
		                array ( 
		                  'project_id' => $project_arr[0]->id, 
		                  'user_id'    => $user_id,
		                  'months'     => 1,
		                  'item'       => 'working capital'
		                ), 
		                array ( 
		                  '%d', 
		                  '%d',
		                  '%d',
		                  '%s'
		                ) 
		            );
				}
			}
		}		
  	}

    if ( ! is_admin() ) {
    	if($_GET) { 		
    		if(!empty($_GET['act'])) {
    			$key = $_GET['act'];
				$res = $wpdb->get_var($wpdb->prepare("SELECT EXISTS(SELECT 1 FROM {$wpdb->users} WHERE user_activation_key = %s)", $key));

				if ($res) {
					$user_id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM {$wpdb->users} WHERE user_activation_key = %s", $key));
					if ( is_user_logged_in() )
  					{
  						if($current_user->ID != $user_id) {
  							wp_redirect(esc_url( home_url( '/' ) ));
  							exit;
  						}
  					}  		  						

					$user = get_userdata( $user_id );
					// Generate something random for a password reset key.
			        $key = wp_generate_password( 20, false );	

			        /** This action is documented in wp-login.php */
       				do_action( 'retrieve_password_key', $user->user_login, $key );		 			        
			        // Now insert the key, hashed, into the DB.
			        if ( empty( $wp_hasher ) ) {
			            require_once ABSPATH . WPINC . '/class-phpass.php';
			            $wp_hasher = new PasswordHash( 8, true );
			        }

			        $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
			        $update_r = $wpdb->update ( 
			        	$wpdb->users, 
			        	array( 'user_activation_key' => $hashed ), 
			        	array( 'user_login' => $user->user_login )
			        );
			        if($update_r) {
			        	update_user_meta($user_id, 'user_email_varify', 'true');
			        	if(!empty($user->user_nicename)){
				            //$message = '<p><b>'.__($user->user_nicename)."</b></p>\r\n\r\n";
				        } else {
				            //$message = '<p><b>'.__($user->user_login)."</b></p>\r\n\r\n";
				        }

				        $message .= '<table cellspacing="0" cellpadding="0" border="0" style="height:100%;margin:auto;width:100%;font-family:sans-serif;background-position:0 -50px;background-size:auto;background-repeat:repeat-x; max-width:850px;background-color: #36d76a;">
							<tr>
								<td valign="top">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td valign="top" style="padding: 40px 50px 40px 50px;">
												<table cellspacing="0" cellpadding="0" border="0" style="margin: auto">
													<tr>	
														<td valign="top" style="padding-right: 30px;">
															<img src="https://bizskillsacademy.com/wp-content/uploads/2017/07/email_box.png" alt="bizskillsacademy" />
														</td>							
														<td valign="top" style="text-align: left;">
															<img src="https://bizskillsacademy.com/wp-content/uploads/2016/11/Logo-white.png" alt="bizskillsacademy" />
														</td>
													</tr>						
												</table>
											</td>
										</tr>
										<tr>
											<td valign="top" style="padding: 0px 50px 0px 50px; ">		
											<div style="font-family:sans-serif; width:100%;background-color:#fff;padding:30px 5%;float:left;display:inline-block;box-sizing:border-box;min-height:100px;color: #4c4c4c;text-align: center;font-size: 21px;">';
      					
      					$message .= '<p style="float: left;width: 100%;">'.__('Thank you for subscribing to BizSkills Academy. We are excited to have you on board.')."</p>\r\n\r\n";
      					//$message .= '<p style="color:#39a04a;float: left;width: 100%;">'.__('<b>"Keep the passion and the vision alive, it is what makes every single millisecond worth living"</b>')."</p>\r\n\r\n";
      					$message .= '<p style="float: left;width: 100%;">'.sprintf(__('Your username is: <b>%s</b>'), $user->user_login) . __( ' and you must click the link below to login:' )."</p>\r\n\r\n";

      					$message .= '<p style="float: left;width: 100%;"><a style="float: none;padding: 8px 10px;display: inline-block;margin: auto;text-align: center;left: 0;right: 0;position: relative; border: 1px solid #36d76a; text-decoration: none;color: #fff;background-color: #36d76a;min-width: 185px;font-weight: 600;" href="'.network_site_url('wp-login.php', 'login').'">Login Link</a></p>';
      					
      					$message .= '<p style="float: left;width: 100%;"><h3><b>'.__('You must also follow these steps to setup your account successfully:')."</b></h3></p>\r\n\r\n";
      					$message .= '<ul style="list-style-type: decimal;text-align: left;">';
      					$message .= '<li style="margin-bottom: 5px;">'.__('Key in your username or email address used to register.')."</li>";
      					$message .= '<li style="margin-bottom: 5px;">'.__('Key in your password and <b><u>sign in</u></b>')." </li>";
      					$message .= '<li style="margin-bottom: 5px;">'.__('Fill in the information on the “My Business Data” page, where you must customize your BizSkills Academy account data according to your country and business. To work on this page, you must have information about the monetary currency, taxable income rate, the business name and the Date on which you plan to start your business.')."</li>";      				
      					$message .= '<li style="margin-bottom: 5px;">'.__('After the above information has been filled, click on the 
      						<u><b>“Submit”</b></u> button below the above information')."</li>";
      					$message .= '<li style="margin-bottom: 5px;">'.__('Click on <u><b>Financial</b></u> Planning to enter the software.')."</li>";
      					$message .= '<li style="margin-bottom: 5px;">'.__('Give your account enough time for the system to <u><b>create a cache</b></u> for your work. This may take a few minutes and it usually makes the system go a bit slow. After this process, the system will work very fast.')."</li>";
      					$message .= '<li style="margin-bottom: 5px;">'.__('Follow the video instructions given on how to work with the system and how to work with each page. Always watch a video to the end, and if it is not clear, watch it again and again until you get it right.')."</li>";
      					$message .= '<li style="margin-bottom: 5px;">'.__('Always sign-out of your account, especially if you are not using your own laptop.')."</li>";
      					$message .= "</ul>\r\n\r\n";
      					
      					$message .= '<p style="float: left;width: 100%;">'.__('Thank you for choosing to work on your project with BizSkills Academy, we are always here. If you need support don’t hesitate to send us your request from the platform support link!')."</p>\r\n\r\n";

      					$message .= '</div>		
										</td>
									</tr>
									<tr>
										<td valign="top" style="padding: 35px 50px 35px 50px;">
											<p style="text-align: center;"><a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">wwww.bizskillsacademy.com</a></p>
											<p style="font-family: sans-serif;text-align: center;color: #fff;">&copy; '.date("Y").' <a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">bizskillsacademy.com</a> - All Rights Reserved</p>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>';

      					$blogname = wp_specialchars_decode(get_option('blogname'),ENT_QUOTES);
      					$return_r = wp_mail($user->user_email, sprintf(__('Welcome to %s'), $blogname), $message);
			        }
				} else {
					wp_redirect(esc_url( home_url( '/' ) ));
				}
    		} else {
				//wp_redirect(esc_url( home_url( '/' ) ));
			}
    	}
    }
}

add_action( 'user_register', 'usr_registration_meta_save', 10, 1 );
function usr_registration_meta_save( $user_id ) {
	global $wpdb;
	if ( ! is_admin() ) {
	    $email_varify = get_user_meta($user_id, 'user_email_varify', true);
	    if(empty($email_varify)) {	    	
	    	add_user_meta( $user_id, 'user_email_varify', 'false', true );	
	    } else {
	    	if($email_varify != 'true'){
	    		update_user_meta( $user_id, 'user_email_varify', 'false' );
	    	}	    	
	    }	
	}
}

add_action( 'wp_authenticate' , 'check_custom_authentication' );
function check_custom_authentication ( $username ) {
    global $wpdb;
    $user;
    if ( ! is_admin() ) {
	    $error = new WP_Error();
	    if (!filter_var($username, FILTER_VALIDATE_EMAIL) === false) {	  		  	
		    if( !email_exists($username) ) {	    	
		        return;
		    }
		    $user = get_user_by( 'email', $username );
		} else {
			if( ! username_exists( $username ) ) {
		    	return;
		   	}
		   	$user = get_user_by( 'login', $username );
		}	
		$email_varify = get_user_meta($user->ID, 'user_email_varify', true);	
		
		if(empty($email_varify)) { 		
			add_user_meta( $user->ID, 'user_email_varify', 'false', true );
			//return;
		} else {		
			if(!is_array($email_varify)) 
			{			
				if($email_varify == 'false')
				{			
					$error->add( 'email_varify', __('<strong>ERROR</strong>: Your email is not verified.' ) );					
					if (!filter_var($username, FILTER_VALIDATE_EMAIL) === false) {	
				 		remove_action('authenticate', 'wp_authenticate_email_password', 20);
				 	} else {
				 		remove_action('authenticate', 'wp_authenticate_username_password',20);
				 	}

					//return $error;	
					//exit;
					return $error;	
				} 
			} 	
		}	
	}
}

add_filter( 'authenticate', 'myplugin_auth_signon', 30, 3 );
function myplugin_auth_signon( $user, $username, $password ) {
    global $wpdb;
    if ( is_a($user, 'WP_User') )
        return $user;

    if ( empty( $username ) || empty( $password ) ) {
        if ( is_wp_error( $user ) )
            return $user;

        $error = new WP_Error();

        if ( empty( $username ) )
            $error->add( 'empty_username', __('<strong>ERROR</strong>: The username field is empty.' ) );

        if ( empty( $password ) )
            $error->add( 'empty_password', __( '<strong>ERROR</strong>: The password field is empty.' ) );

        return $error;
    }
    if ( ! is_admin() ) {
	    if (!filter_var($username, FILTER_VALIDATE_EMAIL) === false) {	  		  	
		    if( !email_exists($username) ) {	    	
		        return;
		    }
		    $user = get_user_by( 'email', $username );
		} else {
			if( ! username_exists( $username ) ) {
		    	return;
		   	}
		   	$user = get_user_by( 'login', $username );
		}	
		$email_varify = get_user_meta($user->ID, 'user_email_varify', true);	
		
		if(empty($email_varify)) { 		
			add_user_meta( $user->ID, 'user_email_varify', 'false', true );
			//return;
		} else {		
			if(!is_array($email_varify)) 
			{			
				if($email_varify == 'false')
				{	
					$script = '<script type="text/javascript" defer>
						jQuery(document).ready(function( $ ) {
							$( document ).delegate( "#send_email_varify", "click", function() {
							  	var request = $.ajax({
									url: "'.admin_url('admin-ajax.php').'",
								  	method: "POST",
								  	data: { 
								  		action: "send_activation_email", 
								  		id: '.$user->ID.' 
								  	},
								  	dataType: "json"
								});
								 
								request.done(function( msg ) {
								  	if(msg.rtn) {
								  		alert("Verification email sent. Please check your email");
								  		location.assign(window.location.href);
								  	} else {
								  		alert(msg.rtn_text);
								  		location.assign(window.location.href);
								  	}
								});
								 
								request.fail(function( jqXHR, textStatus ) {
								  	//alert( "Request failed: " + textStatus );
								});
							});
						});
					</script>';	
				 	$error = new WP_Error();		
				 	$error->add( 'email_varify', __('<strong>ERROR</strong>: Your email is not verified.<br>If you didn‘t receive your verification <a id="send_email_varify" href="javascript:void(0);">Click here</a>'.$script ) );
				 	if (!filter_var($username, FILTER_VALIDATE_EMAIL) === false) {	
				 		remove_action('authenticate', 'wp_authenticate_email_password', 20);
				 	} else {
				 		remove_action('authenticate', 'wp_authenticate_username_password', 20);
				 	}					
					return $error;						
					//return false;	
				}
			} 	
		}			
	}	
}

add_action( 'wp_ajax_send_activation_email', 'fun_send_activation_email' );
add_action( 'wp_ajax_nopriv_send_activation_email', 'fun_send_activation_email' );
function fun_send_activation_email() {
	$arr_rtn = array();
	global $wpdb, $wp_hasher;
	if(isset($_POST['id']))
	{
		if(!empty($_POST['id'])) 
		{			
	        $user = get_userdata( $_POST['id'] );
	 
	        // The blogname option is escaped with esc_html on the way into the database in sanitize_option
	        // we want to reverse this for the plain text arena of emails.
	        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
	 
	        // Generate something random for a password reset key.
	        $key = wp_generate_password( 20, false );
	 
	        /** This action is documented in wp-login.php */
	        do_action( 'retrieve_password_key', $user->user_login, $key );
	 
	        // Now insert the key, hashed, into the DB.
	        if ( empty( $wp_hasher ) ) {
	            require_once ABSPATH . WPINC . '/class-phpass.php';
	            $wp_hasher = new PasswordHash( 8, true );
	        }

	        $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
	        $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

	        //html code
	        $message .= '<table cellspacing="0" cellpadding="0" border="0" style="height:100%;margin:auto;width:100%;font-family:sans-serif;background-position:0 -50px;background-size:auto;background-repeat:repeat-x; max-width:850px;background-color: #36d76a;">
				<tr>
					<td valign="top">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td valign="top" style="padding: 40px 50px 40px 50px;">
									<table cellspacing="0" cellpadding="0" border="0" style="margin: auto">
										<tr>	
											<td valign="top" style="padding-right: 30px;">
												<img src="https://bizskillsacademy.com/wp-content/uploads/2017/07/email_box.png" alt="bizskillsacademy" />
											</td>							
											<td valign="top" style="text-align: left;">
												<img src="https://bizskillsacademy.com/wp-content/uploads/2016/11/Logo-white.png" alt="bizskillsacademy" />
											</td>
										</tr>						
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" style="padding: 0px 50px 0px 50px; ">		
								<div style="font-family:sans-serif; width:100%;background-color:#fff;padding:30px 5%;float:left;display:inline-block;box-sizing:border-box;min-height:100px;color: #4c4c4c;text-align: center;font-size: 25px;">';

	        if(!empty($user->user_nicename)){
	            $message  .= '<p style="float: left;width: 100%;"><b>'.__($user->user_nicename) . "</b></p>\r\n\r\n";
	        } else {
	            $message  .= '<p style="float: left;width: 100%;"><b>'.__($user->user_login) . "</b></p>\r\n\r\n";
	        }
	    
	        $message  .= '<p style="float: left;width: 100%;">'.__('Before I register you for our service, I want to make sure this is your correct email address. Please click the link below to verify your email address.') . "</p>\r\n\r\n";
	        
	        $verify_url = esc_url( add_query_arg( 'act', $hashed,  get_permalink(1471) ));
	        $message .= '<p style="float: left;width: 100%;">'.sprintf(__('<a style="float: none;padding: 8px 10px;display: inline-block;margin: auto;text-align: center;left: 0;right: 0;position: relative; border: 1px solid #36d76a; text-decoration: none;color: #fff;background-color: #36d76a;min-width: 185px;font-weight: 600;" href="%s">Click Here</a>'), $verify_url)."</p>\r\n\r\n";
	        $message .= '<p style="float: left;width: 100%;">'.__('Clicking the link above will confirm your email address and allow you to receive the login information into your service at BizSkills Academy.')."</p>\r\n\r\n";
	        $message .= '<p style="float: left;width: 100%;">'.__('Cheers, See You Soon!')."</p>\r\n\r\n";
	        $message .= '<p style="float: left;width: 100%;">'.__('BizSkills Academy Management')."</p>\r\n\r\n";
	        $message .= '</div>	
								</td>
							</tr>
							<tr>
								<td valign="top" style="padding: 35px 50px 35px 50px;">
									<p style="text-align: center;"><a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">wwww.bizskillsacademy.com</a></p>
									<p style="font-family: sans-serif;text-align: center;color: #fff;">&copy; 2016 <a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">bizskillsacademy.com</a> - All Rights Reserved</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>';

	        $return_r = wp_mail($user->user_email, sprintf(__('Welcome to %s'), $blogname), $message);
	        if($return_r){
	        	update_user_meta( $user_id, 'email_if_cond', $return_r);
	        	$arr_rtn['rtn'] = true;
				$arr_rtn['rtn_text'] = "Email send.";
	        } else {
	        	update_user_meta( $user_id, 'email_else_cond', $return_r);
	        	$arr_rtn['rtn'] = false;
				$arr_rtn['rtn_text'] = "Error: Email not send.";
	        }
    	} else {
    		$arr_rtn['rtn'] = false;
			$arr_rtn['rtn_text'] = "Error: unexpected.";
    	}
	} else {
		$arr_rtn['rtn'] = false;
		$arr_rtn['rtn_text'] = "Error: unexpected.";
	}
	echo json_encode($arr_rtn);
	die();
}

function change_frontend_editor_iframe_url($url) {
    return str_replace("http://", "https://", $url);
}
add_filter('vc_frontend_editor_iframe_url', 'change_frontend_editor_iframe_url');

add_action('init', function() {
  $url = home_url(add_query_arg(array()));
  $id = url_to_postid($url);
  if ($id) {
	if ( !is_user_logged_in() )
  	{
    	if($id == 19) {
     		$urlr =  esc_url( home_url( '/' ) );
			wp_redirect( $urlr);
			exit;
     	}
 	}
  }
});

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}

function template_files_func( $atts ) {
	global $wpdb;
		
	$atts = shortcode_atts( array (	
		'btcount' => 3,
		'order' => 'ASC',		
	), array_filter($atts), 'template_files' );
	
	if(empty($atts['btcount']) || $atts['btcount'] == 'default') {
		$atts['btcount'] = 3;
	}
	$args = [    
	    'post_type' => 'publication',
	    'posts_per_page' => 3,
	    'orderby' => 'menu_order',
	    'order'   => 'ASC'
	];
	$query = new WP_QUERY($args);
	$rtn_html = '<div class="publication-archive">';
	while ($query->have_posts()) {
    	$query->the_post();
    	$key_1_value = get_post_meta( get_the_ID(), 'key_1', true );
		// Check if the custom field has a value.
		if ( ! empty( $key_1_value ) ) {
		    
		}
		$myvals = get_post_meta(get_the_ID());
    	$rtn_html .= var_dump(myvals);
	}
	wp_reset_postdata();
	$rtn_html = '</div>';
	return $rtn_html;
}
add_shortcode( 'template_files', 'template_files_func' );


add_action( 'wp_ajax_del_project', 'func_del_project' );
add_action( 'wp_ajax_nopriv_del_project', 'func_del_project' );

function func_del_project(){
	$arr_rtn = array();
	global $wpdb;
	global $lms_wpdb;
	if ( ! wp_verify_nonce( $_POST["wpnonce"], $_POST["pid"] )) {
	    // This nonce is not valid.
	    $arr_rtn["rtn"] = false;
	    $arr_rtn["rtn_text"] = "Not allowed";
	} else {
		if ( is_user_logged_in() )
	  	{	      	
	 		$user_id = get_current_user_id();
		   
		    $sel_project = $lms_wpdb->get_row( "SELECT * FROM tbl_project WHERE user_id = ".$user_id." AND id = ".$_POST["pid"], OBJECT );	
		  	if($sel_project) {
		  		$del_return = $lms_wpdb->delete( 
		  			'tbl_project', 
		  			array( 
		  				'id' => $_POST["pid"],
		  				'user_id' => $user_id
		  			), 
		  			array( '%d', '%d' ) 
		  		);
		  		if($del_return){
		  			$arr_rtn["rtn"] = true;
	    			$arr_rtn["rtn_text"] = "Project ".$sel_project->name." has been deleted successfully.";
		  		} else {
		  			$arr_rtn["rtn"] = false;
	    			$arr_rtn["rtn_text"] = "invalid project";	
		  		}
		  	} else {
		  		$arr_rtn["rtn"] = false;
	    		$arr_rtn["rtn_text"] = "invalid project";
		  	}

	  	} else {
	  		$arr_rtn["rtn"] = false;
	    	$arr_rtn["rtn_text"] = "user session is logout";
	  	}
	}
	echo json_encode($arr_rtn);
	die();
}

/*class Nav_Walker extends Walker_Nav_Menu {
	
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$output = $output;
	}
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output = $output;
	}
}*/

function func_feature_artical_grid() 
{
	$var =  '<div class="row latest_posts">';
	//$args = array( 'numberposts' => 10, 'category_name' => 'cat-slug' );
	$args = array( 
		'post_type' => 'post', 
		'posts_per_page' => 3, 
		'cat' => 127, 
		'orderby' => 'date', 
		'order' => 'DESC',
	); 
	$the_query = new WP_Query( $args ); 
	if ( $the_query->have_posts() ) {
		// The Loop
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$title = get_the_title();
			$title = substr($title, 0, 25);
			$content = strip_tags(get_the_content());
			$content = substr($content, 0, 80);
			$date = get_the_date('d M, Y');
			$post_id = get_the_ID();
			/*$url =  get_the_post_thumbnail( $post_id, 'full' ); */
			$image = get_the_post_thumbnail_url(); 
			$image =  get_the_post_thumbnail_url( $post_id, array( 300, 200) );

			$link = get_the_permalink();
			$var .= '  <div class="col-md-4 col-sm-4">
			<div class="thumbnail latest_thumb">
			<a href="'.$link.'">
			<img src="'.$image.'">
			</a>
			<div class="caption">
			<h3 class="latest_grid_title">'.$title.'</h3>
			<p class="latest_grid_color">'.$content.'</p>
			<p class="latest_grid_title">'.$date.'</p>
			</div>
			</a>
			</div>
			</div>';
		}			
		wp_reset_postdata();
	}
	$var .= '</div>';
	return $var;
}
add_shortcode('feature_artical_grid', 'func_feature_artical_grid');

function func_courses_grid2() 
{
	$var =  '<div class="row latest_posts">';
	//$args = array( 'numberposts' => 10, 'category_name' => 'cat-slug' );
	$args = array( 
		'post_type' => 'post', 
		'posts_per_page' => 3, 
		'cat' => 128, 
		'orderby' => 'date', 
		'order' => 'DESC',
	); 
	$the_query = new WP_Query( $args ); 
	if ( $the_query->have_posts() ) {
		// The Loop
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$title = get_the_title();
			$title = substr($title, 0, 30);
			$content = strip_tags(get_the_content());
			$content = substr($content, 0, 400);
			$date = get_the_date('d M, Y');
			$post_id = get_the_ID();
			/*$url =  get_the_post_thumbnail( $post_id, 'full' ); */
			$image = get_the_post_thumbnail_url(); 
			/*$image =  get_the_post_thumbnail_url( $post_id, array( 300, 200) );*/

			$link = get_the_permalink();
			$var .= ' <div class="hover_container">
  <div  class="image-wrap" style="background-image: url('.$image.')">
  <h3>'.$title.'</h3>
  </div>
  
  <div class="middle">
    <div class="text">'.$content.'
    </div>
  </div>
</div>';
		}			
		wp_reset_postdata();
	}
	$var .= '</div>';
	return $var;
}
add_shortcode('courses_grid2', 'func_courses_grid2');

function check_if_cart_has_product( $valid, $product_id, $quantity ) 
{  

		if(!empty(WC()->cart->get_cart()) && $valid){
		    foreach (WC()->cart->get_cart() as $cart_item_key => $values) {
		        $_product = $values['data'];

		        if( $product_id == $_product->get_id() ) {
		            unset(WC()->cart->cart_contents[$cart_item_key]);
		        }
		    }
		}

		return $valid;

}
	add_filter( 'woocommerce_add_to_cart_validation', 'check_if_cart_has_product', 10, 3 );



 
function woo_custom_add_to_cart( $cart_item_data ) {
	global $woocommerce;
	$woocommerce->cart->empty_cart();
	 
	return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'woo_custom_add_to_cart' );

if(isset($_POST['invite_form']))
{

	$current_user = wp_get_current_user();
	$username = $current_user->user_login;
	
	$new_str = str_replace(str_split('[\\/:*?"<>|]'), ' ', $_POST['example_emailBS']);
	$data_array = explode(",",$new_str);
	$user_id = base64_encode(get_current_user_id());
	$discount_coupon = trim($_POST['discount_coupon']);
	
	
	if(!empty($discount_coupon))
	{
		$site_url = site_url()."/#section-5";
	}
	else
	{
		$site_url = site_url()."/#section-5";	
	}

	$count = 0;
	foreach ($data_array as $data)
	{

		$email = trim($data); 
		

		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$count++;
		    $message = '<table cellspacing="0" cellpadding="0" border="0" style="height:100%;margin:auto;width:100%;font-family:sans-serif;background-position:0 -50px;background-size:auto;background-repeat:repeat-x; max-width:850px;background-color: #36d76a;">
		<tr>
			<td valign="top">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td valign="top" style="padding: 40px 50px 40px 50px;">
							<table cellspacing="0" cellpadding="0" border="0" style="margin: auto">
								<tr>	
									<td valign="top" style="padding-right: 30px;">
										<img src="https://bizskillsacademy.com/wp-content/uploads/2017/07/email_box.png" alt="bizskillsacademy" />
									</td>							
									<td valign="top" style="text-align: left;">
										<img src="https://bizskillsacademy.com/wp-content/uploads/2016/11/Logo-white.png" alt="bizskillsacademy" />
									</td>
								</tr>						
							</table>
						</td>
					</tr>
					<tr>
						<td valign="top" style="padding: 0px 50px 0px 50px; ">		
						<div style="font-family:sans-serif; width:100%;background-color:#fff;padding:30px 5%;float:left;display:inline-block;box-sizing:border-box;min-height:100px;color: #4c4c4c;text-align: center;font-size: 21px;">';

		$message .= '<p style="float: left;width: 100%;text-align:left;">'. __( 'Hello there!' )."</p>\r\n\r\n";

		$message .= '<p style="float: left;width: 100%; text-align:left;">'.__($username.' is inviting you to work on your project through BizSkills Academy. Click on the link below to register on this platform so you can start to improve your project immediately. ')."</p>\r\n\r\n";
		//$message .= '<p style="color:#39a04a;float: left;width: 100%;">'.__('<b>"Keep the passion and the vision alive, it is what makes every single millisecond worth living"</b>')."</p>\r\n\r\n";
		if(!empty($discount_coupon))
		{
			$message .= '<p style="float: left;width: 100%;text-align:left;">'. __( 'Your coupon Code is: ' .$discount_coupon)."</p>\r\n\r\n";
		}	

		$message .= '<p style="float: left;width: 100%;"><a style="float: none;padding: 8px 10px;display: inline-block;margin: auto;text-align: center;left: 0;right: 0;position: relative; border: 1px solid #36d76a; text-decoration: none;color: #fff;background-color: #36d76a;min-width: 185px;font-weight: 600;" href="'.$site_url.'">Invitation Link</a></p>';

		
		$message .= '<p style="float: left;width: 100%;text-align:left;">BizSkills Academy has one of the best solutions helping early stage entrepreneurs like yourself. Through this platform, you will be able to develop your project under my guidance, while at the same time empowering you to make the right decisions.</p>';
		
	
		$message .= '<p style="float: left;width: 100%;text-align:left;">This is a great opportunity for us to collaborate, improve your project fast, productively, and most conveniently.</p>';
		
		
		
		$message .= '<p style="float: left;width: 100%;text-align:left;">'.__('Best,')."</p>\r\n\r\n";
		$message .= '<p style="float: left;width: 100%;text-align:left;">'.__($username)."</p>\r\n\r\n";
		$message .= '</div>		
					</td>
				</tr>
				<tr>
					<td valign="top" style="padding: 35px 50px 35px 50px;">
						<p style="text-align: center;"><a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">wwww.bizskillsacademy.com</a></p>
						<p style="font-family: sans-serif;text-align: center;color: #fff;">&copy; '.date("Y").' <a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">bizskillsacademy.com</a> - All Rights Reserved</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>';

$blogname = wp_specialchars_decode(get_option('blogname'),ENT_QUOTES);
$return_r = wp_mail($email, sprintf(__('Welcome to %s'), $blogname), $message); 

		} 
		
	}
	if($count > 0)
	{
		$_SESSION['success']="<h3>Congratulations, invitations sent successfully</h3>";
	}	

}

?>