<?php get_header(); 
global $pagename;
	
 ?>

	<div class="single-blog-custom-sp">

	<section id="content" class="content-section section ">

		<div class="sec_data">
		<div class="container container-table custom_left">
		
		<div class=" container-table">
			<div class="main-section" role="main">
			<?php get_template_part( 'loop_course' ); ?>
			<?php comments_template(); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
		</div>
		</div>
		
	</section>
	</div>
<div class="clear"></div>

	<?php get_footer(); ?>