<?php
/*
 * Template Name: custom about page
 * Description: No Sidebar
 */
get_header('custom'); 

?>
<style>
[data-class="hide"] {
    display:none;
}
[data-class="show"] {
    display:block;
}
.hero-image {
  background-image: url("https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/2.business-advisory.jpg");
   height: 410px;
  background-position: center -20px;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  ;
}
</style>
<div class="hero-image">
  <div class="hero-text">
  	<h2>ABOUT US</h2>
    <p>Turning skills, talents & passions into businesses.</p>
  </div>
</div>
	<?php $is_using_vc = get_post_meta( get_the_ID(), '_wpb_vc_js_status', true ); ?>

<?php if ( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<?php if ( $is_using_vc === 'true' ) : ?>
		<div id="content" <?php post_class( 'visual-composer-page' ); ?>>
			<div class="section-anchor" data-anchor="remove" data-section="#header"></div>
			<?php the_content(); ?>

		</div>

	<?php else : ?>

		<section id="content" class="content-section section">
			<div class="container container-table">

				<div class="main-section" role="main">
					
					<div class="page-loop">

						<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php the_content(); ?>
						</article>

					</div>

					<?php comments_template(); ?>

				</div>

				<?php get_sidebar(); ?>

			</div>
		</section>

	<?php endif; ?>

<?php endwhile; endif; ?>


<?php get_footer(); ?>
