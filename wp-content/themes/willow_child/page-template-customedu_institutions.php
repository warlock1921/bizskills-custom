<?php
/*
 * Template Name: custom hero banner page template
 * Description: No Sidebar
 */
get_header('custom'); 
global $pagename;
$hero_title = rwmb_meta( 'bizskills-hero_title' );
if(empty($hero_title))
{
	$hero_title = $pagename; 
}

$content = rwmb_meta( 'bizskills-hero_content' );
$background_image2 = 'https://bizskillsacademy.com/bizcustom/wp-content/uploads/2018/03/bannet-1.jpg';
$background_image = rwmb_meta( 'bizskills-image_advanced_2' );
if(!empty($background_image))
{
	foreach ( $background_image as $bg ) {
    	$background_image =  $bg['full_url'];
	}
}
else
{
	$background_image = $background_image2;

}	

?>
<style>
[data-class="hide"] {
    display:none;
}
[data-class="show"] {
    display:block;
}
.hero-image {
  background-image: url("<?php echo $background_image; ?>");
  
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  ;
}
</style>
<div class="hero-image">
  <div class="hero-text">
 
    	<h2><?php echo strtoupper($hero_title); ?></h2>
   

  <?php if(!empty($content)) { ?>
    <p><?php echo $content; ?></p>
    <?php } ?>
  </div>
</div>
	<?php $is_using_vc = get_post_meta( get_the_ID(), '_wpb_vc_js_status', true ); ?>

<?php if ( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<?php if ( $is_using_vc === 'true' ) : ?>
		<div id="content" <?php post_class( 'visual-composer-page' ); ?>>
			<div class="section-anchor" data-anchor="remove" data-section="#header"></div>
			<?php the_content(); ?>

		</div>

	<?php else : ?>

		<section id="content" class="content-section section">
			<div class="container container-table">

				<div class="main-section" role="main">
					
					<div class="page-loop">

						<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php the_content(); ?>
						</article>

					</div>

					<?php comments_template(); ?>

				</div>

				<?php get_sidebar(); ?>

			</div>
		</section>

	<?php endif; ?>

<?php endwhile; endif; ?>


<?php get_footer(); ?>
