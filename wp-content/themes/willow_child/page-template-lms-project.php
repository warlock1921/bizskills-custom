<?php
/*
 * Template Name: Full Width lms projects
 * Description: No Sidebar
 */
 	 	
?>

<?php
 	global $lms_wpdb;

  	if ( !is_user_logged_in() )
  	{
      	wp_redirect( esc_url( home_url() ));
      	exit();
  	}
 	$user_id = get_current_user_id();
  	get_header(); 

	if(isset($_POST['btn_create_project']))
	{
		
	 	$sel_project = $lms_wpdb->get_row( "SELECT * FROM tbl_project WHERE user_id = ".$user_id, OBJECT );	
	  	if($sel_project) {
	  		echo '<center><p>'.$_POST['txt_pro_name'].' already exist.</p></center>';       
	  	} else {
	  		$ins_project = $lms_wpdb->insert( 
				'tbl_project', 
				array( 
					'user_id' => $user_id, 
					'name' => $_POST['txt_pro_name'],
					'currency' => $_POST['currency']
				), 
				array( 
					'%d', 
					'%s',
					'%s'
				) 
			);
			if($ins_project){
				echo '<center><p>Lms Project '.$_POST['txt_pro_name'].' has been created successfully.</p></center>';  
			} else {
				echo '<center><p>Error: Please try again.</p></center>'; 
			}
	  	}
	}
?>
  <script type="text/javascript">
    jQuery(document).ready(function( $ ) {
        $( "#lms_project" ).submit(function( event ) {
            if($('#currency').val()=='0') {
              $('#currency').focus();
              return false;
            }
            else if($('#txt_pro_name').val()=='') {
              $('#txt_pro_name').focus();
              return false;
            }
            else {
              return true;
            }                                 
        });
        var del_act = false;
        $( ".del-project" ).click(function() {
        	if(del_act) {
        		return false;
        	}
        	var resp = confirm("Do you want to delete this Project?");
		   	if (resp == true) {
		   		del_act = true;
		      	//do something		   		
			  	if($(this).data("pid")) {
			  		var proid = $(this).data("pid");
			  		var wpnonce = $(this).data("nonce");
			  		var request = $.ajax({
			            url: ajaxurl,
			            method: "POST",
			            data: { 
			                action : "del_project",
			                pid: proid,
			                wpnonce: wpnonce
			            },
			            dataType: "json",
			            beforeSend: function( xhr ) {     
			           		$('#lms-project-con').css({ opacity: 0.6 });
			            }
			        });      
			        request.done(function( msg ) {
			        	if(msg.rtn) {
			        		alert(msg.rtn_text);
			        		window.location.reload();
			        	} else {
			        		alert(msg.rtn_text);
			        		window.location.reload();
			        	}
			        });
			        request.complete(function(xhr) {
			        	$('#lms-project-con').css({ opacity: 1 });
				        //$(".loading-slideshow").fadeOut("slow", function() {
				        //$(".loading-slideshow").remove();
				        //});
				    });
				    request.fail(function( jqXHR, textStatus ) {
				        console.log( "Request failed: " + textStatus );
				        window.location.reload();
				    });
			  	}	
		  	}	  	
		});       
    });
  </script>

<div id="content" <?php post_class( 'visual-composer-page' ); ?>>
	<div class="section-anchor" data-anchor="remove" data-section="#header"></div>
	<div id="lms-project-con" class="container business_profile">
		<div class="project-delete-con">

		<?php
			$sel_project = $lms_wpdb->get_results( "SELECT * FROM tbl_project WHERE user_id = ".$user_id, OBJECT );	
	  		if($sel_project) {
	  	?>
	  		<table cellpadding="0" cellspacing="0" rules="all" border="0">
	  		<thead>
	  			<tr>
	  			<th>Project</th>
	  			<th>Currency</th>
	  			<th>Action</th>
	  			</tr>
	  		</thead>
	  		<tbody>
	  		
	  	<?php
	  			foreach ( $sel_project as $project_row ) 
              	{

              	//echo '<option value="'.$project_row->id.'">'.$project_row->name.'</option>';
              		//esc_url( get_permalink(1534) 
	  	?>		<tr>
		  			<td><a href="<?php echo get_permalink(1534); ?>?pid=<?php echo $project_row->id; ?>"><?php echo $project_row->name; ?></a></td>
		  			<td><?php echo $project_row->currency; ?></td>
		  			<td><a class="del-project" 
		  			data-pid="<?php echo $project_row->id; ?>" 
		  			data-nonce="<?php echo wp_create_nonce( $project_row->id ); ?>"
		  			href="javascript:void(0);">Delete</a></td>
	  			</tr>
	  	<?php
				}
		?>
			</tbody>
			</table>
		<?php
	  		} else {
	  	?>
	  		<script type="text/javascript"> 
			    window.location.assign("<?php echo get_permalink(1534); ?>");
			</script>
	  	<?php
	  		}
		?>		
		</div>
	</div>
</div>

<?php get_footer(); ?>
