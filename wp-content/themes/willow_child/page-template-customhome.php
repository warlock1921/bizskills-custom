<?php
/*
 * Template Name: custom home page
 * Description: No Sidebar
 */
get_header('custom'); 

?>
<style>
[data-class="hide"] {
    display:none;
}
[data-class="show"] {
    display:block;
}
</style>
	<?php $is_using_vc = get_post_meta( get_the_ID(), '_wpb_vc_js_status', true ); ?>

<?php if ( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<?php if ( $is_using_vc === 'true' ) : ?>

		<div id="content" <?php post_class( 'visual-composer-page' ); ?>>

			<div class="section-anchor" data-anchor="remove" data-section="#header"></div>

			<?php the_content(); ?>

		</div>

	<?php else : ?>

		<section id="content" class="content-section section">
			<div class="container container-table">

				<div class="main-section" role="main">
					
					<div class="page-loop">

						<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php the_content(); ?>
						</article>

					</div>

					<?php comments_template(); ?>

				</div>

				<?php get_sidebar(); ?>

			</div>
		</section>

	<?php endif; ?>

<?php endwhile; endif; ?>
<script>
$( document ).ready(function() {

	
	$(".slide1").removeClass('hide');

	 $("#quote-carousel").on('slid.bs.carousel', function () {
	 	var totalItems = $('.item').length;
   		var currentIndex = $('div.active').index() + 1;
   		
   		if(currentIndex%4 == 1)
   		{
   			var last_slide = parseInt(currentIndex)-1;
   			last_slide = parseInt(last_slide)/4;	
  			var slide_to_show = parseInt(last_slide)+1;

  			var new_class="slide"+slide_to_show;
  		
  			$('.slide_li').addClass("hide");	
  			$("."+new_class).removeClass('hide');
  			
  		}
  		else
  		{
  			if(currentIndex <= 4)
  			{

  				$('.slide_li').addClass("hide");	
  				$(".slide1").removeClass('hide');	
  			}	
  			else
  			{
  				var slide_number = parseInt(currentIndex)/4;
  				var intvalue = Math.ceil(slide_number); 
  				
  				$('.slide_li').addClass("hide");	
  				$(".slide"+intvalue).removeClass('hide');	

  			}	
  		}	
   	});
});
</script>

<?php get_footer(); ?>
