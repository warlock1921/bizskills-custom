<?php
/*
Plugin Name: Custom user change pass email Notification
Description: Changes the copy in the email sent out to new users
Author: onclick.mak
*/

if ( ! has_filter( 'password_change_email', 'change_password_mail_message' ) ) 
{
	add_filter( 'password_change_email', 'change_password_mail_message', 10, 3 );

	function change_password_mail_message( $pass_change_mail, $user, $userdata ) 
	{
		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
		$subject = sprintf( __('%s Notice of Password Change'), $blogname );

		$new_message_txt = '<table cellspacing="0" cellpadding="0" border="0" style="height:100%;margin:auto;width:100%;font-family:sans-serif;background-position:0 -50px;background-size:auto;background-repeat:repeat-x; max-width:850px;background-color: #36d76a;">
			<tr>
				<td valign="top">
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td valign="top" style="padding: 40px 50px 40px 50px;">
								<table cellspacing="0" cellpadding="0" border="0" style="margin: auto">
									<tr>	
										<td valign="top" style="padding-right: 30px;">
											<img src="https://bizskillsacademy.com/wp-content/uploads/2017/07/email_box.png" alt="bizskillsacademy" />
										</td>							
										<td valign="top" style="text-align: left;">
											<img src="https://bizskillsacademy.com/wp-content/uploads/2016/11/Logo-white.png" alt="bizskillsacademy" />
										</td>
									</tr>						
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" style="padding: 0px 50px 0px 50px; ">		
							<div style="font-family:sans-serif; width:100%;background-color:#fff;padding:30px 5%;float:left;display:inline-block;box-sizing:border-box;min-height:100px;color: #4c4c4c;text-align: center;font-size: 21px;">';
		$new_message_txt .= __( '<p style="float: left;width: 100%;"><b>Hi ###USERNAME###,</b></p><p></p>
							<p style="float: left;width: 100%;">This notice confirms that your password was changed on ###SITENAME###.</p>
							<p style="float: left;width: 100%;">If you did not change your password, please contact the Site Administrator at ###ADMIN_EMAIL### is supposed to be redirected to support@bizskillsacademy.com.</p>
							<p style="float: left;width: 100%;">This email has been sent to ###EMAIL### </p>
							<p style="float: left;width: 100%;">Regards,</p>
							<p style="float: left;width: 100%;">Security at BizSkills Academy
							</p>
							<a style="float: none;padding: 8px 10px;display: inline-block;margin: auto;text-align: center;left: 0;right: 0;position: relative; border: 1px solid #36d76a; text-decoration: none;color: #fff;background-color: #36d76a;min-width: 185px;font-weight: 600;" href="###SITEURL###">bizskillsacademy</a>' );
		$new_message_txt .= '</div>		
						</td>
						</tr>
						<tr>
							<td valign="top" style="padding: 35px 50px 35px 50px;">
								<p style="text-align: center;"><a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">wwww.bizskillsacademy.com</a></p>
								<p style="font-family: sans-serif;text-align: center;color: #fff;">&copy; '.date("Y").' <a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">bizskillsacademy.com</a> - All Rights Reserved</p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>';

	  	$headers  = "From: bizskillsacademy < info@bizskillsacademy.com >\n";
		$headers .= "Cc: bizskillsacademy < info@bizskillsacademy.com >\n";
		$headers .= "X-Sender: bizskillsacademy < info@bizskillsacademy.com >\n";
		$headers .= 'X-Mailer: PHP/' . phpversion();
		$headers .= "X-Priority: 1\n"; // Urgent message!
		$headers .= "Return-Path: info@bizskillsacademy.com\n"; // Return path for errors
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=iso-8859-1\n";

	  	$pass_change_mail[ 'message' ] = $new_message_txt;
	  	$pass_change_mail[ 'subject' ] = $subject;
	  	$pass_change_mail[ 'headers' ] = $headers;
	  	
	  	//user_email
	  	return $pass_change_mail;
	}
}


if ( !function_exists('wp_password_change_notification') ) :
/**
 * Notify the blog admin of a user changing password, normally via email.
 *
 * @since 2.7.0
 *
 * @param WP_User $user User object.
 */
function wp_password_change_notification( $user ) {
	// send a copy of password change notification to the admin
	// but check to see if it's the admin whose password we're changing, and skip this
	if ( 0 !== strcasecmp( $user->user_email, get_option( 'admin_email' ) ) ) {
		/* translators: %s: user name */
		$message = '<table cellspacing="0" cellpadding="0" border="0" style="height:100%;margin:auto;width:100%;font-family:sans-serif;background-position:0 -50px;background-size:auto;background-repeat:repeat-x; max-width:850px;background-color: #36d76a;">
			<tr>
				<td valign="top">
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td valign="top" style="padding: 40px 50px 40px 50px;">
								<table cellspacing="0" cellpadding="0" border="0" style="margin: auto">
									<tr>	
										<td valign="top" style="padding-right: 30px;">
											<img src="https://bizskillsacademy.com/wp-content/uploads/2017/07/email_box.png" alt="bizskillsacademy" />
										</td>							
										<td valign="top" style="text-align: left;">
											<img src="https://bizskillsacademy.com/wp-content/uploads/2016/11/Logo-white.png" alt="bizskillsacademy" />
										</td>
									</tr>						
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" style="padding: 0px 50px 0px 50px; ">		
							<div style="font-family:sans-serif; width:100%;background-color:#fff;padding:30px 5%;float:left;display:inline-block;box-sizing:border-box;min-height:100px;color: #4c4c4c;text-align: center;font-size: 21px;">';
					$message .= sprintf( __( '<p style="float:left;width:100%;">Password changed for user: %s </p>' ), $user->user_login ) . "\r\n";
					$message .= '</div>		
						</td>
						</tr>
						<tr>
							<td valign="top" style="padding: 35px 50px 35px 50px;">
								<p style="text-align: center;"><a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">wwww.bizskillsacademy.com</a></p>
								<p style="font-family: sans-serif;text-align: center;color: #fff;">&copy; '.date("Y").' <a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">bizskillsacademy.com</a> - All Rights Reserved</p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>';
		// The blogname option is escaped with esc_html on the way into the database in sanitize_option
		// we want to reverse this for the plain text arena of emails.
		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
		/* translators: %s: site title */
		wp_mail( get_option( 'admin_email' ), sprintf( __( '%s Password Changed' ), $blogname), $message);
	}
}
endif;
?>