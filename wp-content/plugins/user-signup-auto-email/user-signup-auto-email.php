<?php
/*
Plugin Name: Custom New User Singup Email
Description: Changes the copy in the email sent out to new users
Author: onclick.mak
*/

if ( !function_exists('wp_new_user_notification') ) {
 
    function wp_new_user_notification( $user_id, $deprecated = null, $notify = 'both' ) {
        if ( $deprecated !== null ) {
            _deprecated_argument( __FUNCTION__, '4.3.1' );
        }
 
        global $wpdb, $wp_hasher;
        $user = get_userdata( $user_id );
 
        // The blogname option is escaped with esc_html on the way into the database in sanitize_option
        // we want to reverse this for the plain text arena of emails.
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
 
        $message  = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
        $message .= sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
        $message .= sprintf(__('E-mail: %s'), $user->user_email) . "\r\n";
 
        @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), $blogname), $message);
 
        if ( 'admin' === $notify || empty( $notify ) ) {
            return;
        }
 
        // Generate something random for a password reset key.
        $key = wp_generate_password( 20, false );
 
        /** This action is documented in wp-login.php */
        do_action( 'retrieve_password_key', $user->user_login, $key );
 
        // Now insert the key, hashed, into the DB.
        if ( empty( $wp_hasher ) ) {
            require_once ABSPATH . WPINC . '/class-phpass.php';
            $wp_hasher = new PasswordHash( 8, true );
        }

        $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
        $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

        //email html
        $message = '<table cellspacing="0" cellpadding="0" border="0" style="height:100%;margin:auto;width:100%;font-family:sans-serif;background-position:0 -50px;background-size:auto;background-repeat:repeat-x; max-width:850px;background-color: #36d76a;">
    <tr>
        <td valign="top">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                    <td valign="top" style="padding: 40px 50px 40px 50px;">
                        <table cellspacing="0" cellpadding="0" border="0" style="margin: auto">
                            <tr>    
                                <td valign="top" style="padding-right: 30px;">
                                    <img src="https://bizskillsacademy.com/wp-content/uploads/2017/07/email_box.png" alt="bizskillsacademy" />
                                </td>                           
                                <td valign="top" style="text-align: left;">
                                    <img src="https://bizskillsacademy.com/wp-content/uploads/2016/11/Logo-white.png" alt="bizskillsacademy" />
                                </td>
                            </tr>                       
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="padding: 0px 50px 0px 50px; ">      
                    <div style="font-family:sans-serif; width:100%;background-color:#fff;padding:30px 5%;float:left;display:inline-block;box-sizing:border-box;min-height:100px;color: #4c4c4c;text-align: center;font-size: 21px;">';

        if(!empty($user->user_nicename)){
            $message  .= '<p style="float: left;width: 100%;"><b>'.__($user->user_nicename) . "</b></p>\r\n\r\n";
        } else {
            $message  .= '<p style="float: left;width: 100%;"><b>'.__($user->user_login) . "</b></p>\r\n\r\n";
        }
        $message  .= '<p style="float: left;width: 100%;">'.__('Before I register you for our service, I want to make sure this is your correct email address. Please click the link below to verify your email address.')."</p>\r\n\r\n";
        $verify_url = esc_url( add_query_arg( 'act', $hashed,  get_permalink(1471) ));
        $message .= '<p style="float: left;width: 100%;">'.sprintf(__('<a style="float: none;padding: 8px 10px;display: inline-block;margin: auto;text-align: center;left: 0;right: 0;position: relative; border: 1px solid #36d76a; text-decoration: none;color: #fff;background-color: #36d76a;min-width: 185px;font-weight: 600;" href="%s">Click Here</a>'), $verify_url)."</p>\r\n\r\n";
        $message .= '<p style="float: left;width: 100%;">'.__('Clicking the link above will confirm your email address and allow you to receive the login information into your service at BizSkills Academy.')."</p>\r\n\r\n";
        $message .= '<p style="float: left;width: 100%;">'.__('Cheers, See You Soon!')."</p>\r\n\r\n";
        $message .= '<p style="float: left;width: 100%;">'.__('BizSkills Academy Management')."</p>\r\n\r\n";

        $message .= '</div>     
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding: 35px 50px 35px 50px;">
                                <p style="text-align: center;"><a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">wwww.bizskillsacademy.com</a></p>
                                <p style="font-family: sans-serif;text-align: center;color: #fff;">&copy; '.date("Y").' <a style="color: #fff;text-decoration: none;outline: none;border: 0;font-family: sans-serif;" href="https://bizskillsacademy.com/">bizskillsacademy.com</a> - All Rights Reserved</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';


        $return_r = wp_mail($user->user_email, sprintf(__('Welcome to %s'), $blogname), $message);
        if($return_r){
        	add_user_meta( $user_id, 'email_if_cond', $return_r);
        } else {
        	add_user_meta( $user_id, 'email_else_cond', $return_r);
        }
    }
}

?>