=== Website Toolbox Chat Room ===
Contributors: websitetoolbox
Tags: chatroom, chat room, chat, chat room hosting, saas chat room, cloud chat room, website chat room, group chat, inline chat, embedded chat room, embeddable chat room
Requires at least: 3.0.0
Tested up to: 4.9
Stable Tag: 1.1.2

Website Toolbox is the easiest way to create a powerful Chat Room. This plugin embeds your Website Toolbox Chat Room and integrates single sign on.

== Description ==

Not a Website Toolbox Chat Room owner? <a href="https://www.websitetoolbox.com/chat_room/index.html#wordpress">Create a Chat Room Now!</a>

Website Toolbox allows you to easily add a Chat Room to your website without any coding or software headaches! The Website Toolbox Chat Room WordPress plugin is the easiest way to integrate a Website Toolbox Chat Room into your WordPress site.

Ranked #1 by StarReviews!

 = Plugin Features =

* Login Integration: Automatically logs the user into your Website Toolbox Chat Room when they login to your WordPress website.

* Registration Integration: Automatically creates a Website Toolbox Chat Room user account when a user registers on your WordPress website.

* Embedded Chat Room: The Website Toolbox Chat Room is automatically embedded into the layout of your WordPress website.


= Key Chat Room Benefits =

* Instant Setup
* Phone/Chat/Email Support
* Mobile friendly
* Public or Private
* White Label


 = Key Chat Room Features =

* Embeddable
* Multiple Rooms
* Private Messaging
* Searchable Message Archive
* Emoticons
* User Accounts
* Administrators
* Guest Users

<a href="https://www.websitetoolbox.com/chat_room/index.html#wordpress">Learn More</a>

== Installation ==

### WordPress Admin Panel

1. Go to "Plugins" -> "Add New" from the left menu in the WordPress Admin Panel.
2. Search for "Website Toolbox Chat Room"
3. Activate the plugin.
4. <a href="https://www.websitetoolbox.com/chat_room/index.html#wordpress" target="_blank">Create a Website Toolbox Chat Room</a> if you don't already have one.
5. Click on the Settings link under the Website Toolbox Chat Room plugin.
6. Enter your Website Toolbox account information.

Please <a href="https://www.websitetoolbox.com/contact?subject=WordPress+Chat+Room+Plugin+Setup+Help">contact Customer Support</a> if you face any difficulties.

### Manual Installation

1. Download the plugin.
2. Upload the plugin folder to the `/wp-content/plugins/` directory.
3. Activate the plugin through the 'Plugins' menu in WordPress
4. <a href="https://www.websitetoolbox.com/chat_room/index.html#wordpress" target="_blank">Create a Website Toolbox Chat Room</a> if you don't already have one.
5. Click on the Settings link under the Website Toolbox Chat Room plugin.
6. Enter your Website Toolbox account information.

== Changelog ==

= 1.1.2 =
* Changed remember me cookie time to match wordpress default of 15 days

= 1.1.1 =
* Changed the location of the Settings link
* Improved the design of the Embed check box
* Linked the plugin author to our website

= 1.1.0 =
* Uses a fallback SSO method for Safari
* Automatically saves SSO and embed page settings
