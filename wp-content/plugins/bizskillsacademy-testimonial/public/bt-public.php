<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/public
 * @author     Your Name <email@example.com>
 */
class bt_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		add_shortcode( 'biz_testimonial', array( $this, 'biz_testimonial_func' ) );
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bt-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bt-flags', plugin_dir_url( __FILE__ ) . 'css/bt-flags.css', array(), 
			$this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bt-public.js', array( 'jquery' ), $this->version, false );

	}

	function add_my_post_types_to_query( $query ) {
		if ( is_home() && $query->is_main_query() )
			$query->set( 'post_type', array( 'post', 'biztestimonial' ) );
		return $query;
	}

	function biz_testimonial_func( $atts ) {
		global $wpdb;
		
		$atts = shortcode_atts( array (
			'backcolor' => '#fff',
			'btcount' => -1,
			'order' => 'ASC',
			'textlimit' => 200
		), array_filter($atts), 'biz_testimonial' );
		
		if(empty($atts['btcount']) || $atts['btcount'] == 'default') {
			$atts['btcount'] = -1;
		}
		$args = array (
		    'post_type'=> 'biz_testimonial',		    
		    'order'    => $atts['order'],
		    'orderby'  => 'date',
		    'posts_per_page' => $atts['btcount'],
		);

		$the_query = new WP_Query( $args );
		$rtn_html = '<ul class="bt-text-con" style="background-color: '.$atts['backcolor'].'">';

		if($the_query->have_posts()) {

			while ( $the_query->have_posts() ) : $the_query->the_post(); 

				$postid = get_the_ID();
				$rtn_html .= '<li>';
				if ( has_post_thumbnail() ) {
					$rtn_html .= '<div class="bt-image">';
					$rtn_html .= get_the_post_thumbnail($postid,'thumbnail',array('class'=>'center'));
					$rtn_html .= '</div>';
					$rtn_html .= '<div class="bt-text"><i>';
				} else {
					$rtn_html .= '<div class="bt-text" style="width:100%;"><i>';
				}
				$content   = get_the_content();
				//$rtn_html .= wp_strip_all_tags( substr(get_the_content(), 0, $atts['textlimit'] ));
				//$rtn_html .= wp_strip_all_tags(get_the_content());
                                $content   = get_the_content();
				$testimonial_items = apply_filters('the_content', $content);
				$rtn_html .= $testimonial_items;
				$rtn_html .= '</i></div>';

				$rtn_html .= '<div class="bt-author">';

				if(get_post_meta($postid, '_bt_author_name', FALSE)) {
					$bt_author_name = get_post_meta( $postid, '_bt_author_name', true );
					if(!empty($bt_author_name)) {
						$rtn_html .= '<p class="bt_author_name"><i>'.$bt_author_name.'</i></p>';
					} else {
						$rtn_html .= '<p class="bt_author_name"><i>'.get_the_author().'</i></p>';
					}
				} else {
					$rtn_html .= '<p class="bt_author_name"><i>'.get_the_author().'</i></p>';
				}				
				
				if(get_post_meta($postid, '_bt_author_website', FALSE)) {
					$bt_author_website = get_post_meta($postid, '_bt_author_website', true );
					$rtn_html .= '<p class="bt_author_website"><a href="'.$bt_author_website.'">'.
					parse_url($bt_author_website, PHP_URL_HOST).'</a></p>';
				} 

				if(get_post_meta($postid, '_bt_country_name', FALSE)) { 
					$_hdn_country_name = get_post_meta( $postid, '_bt_country_name', true );
					if(!empty($_hdn_country_name)){
						$rtn_html .= '<p class="hdn_country_name">';
						if(get_post_meta($postid, '_bt_author_country', FALSE)) { 
							$bt_author_country = get_post_meta( $postid, '_bt_author_country', true );	
							$rtn_html .= '<span class="flag flag-'.strtolower($bt_author_country).'"></span>';
						}
						$rtn_html .= $_hdn_country_name.'</p>';
					}
				}
				
				$rtn_html .= '</div>';
				$rtn_html .='</li>';

			endwhile;
		}
wp_reset_postdata();
		$rtn_html .= '</ul>';
		return $rtn_html;
	}	
}
