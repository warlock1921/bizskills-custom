<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 * @author     Your Name <email@example.com>
 */
class bt_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */

	private $post_type;

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->post_type = 'biztestimonial';
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bt-admin.css', array(), 
			$this->version, 'all' );

		wp_enqueue_style( 'bt-flags', plugin_dir_url( __FILE__ ) . 'css/bt-flags.css', array(), 
			$this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/plugin-name-admin.js', array( 'jquery' ), $this->version, false );

	}

	function set_custom_post_init() {
		$labels = array (
			'name'               => _x( 'Biz Testimonial', 'post type general name', 'bizskillsacademy-testimonial' ),
			'singular_name'      => _x( 'Biz Testimonial', 'post type singular name', 'bizskillsacademy-testimonial' ),
			'menu_name'          => _x( 'Biz Testimonials', 'admin menu', 'bizskillsacademy-testimonial' ),
			'name_admin_bar'     => _x( 'Biz Testimonial', 'add new on admin bar', 'bizskillsacademy-testimonial' ),
			'add_new'            => _x( 'Add New', 'Biz Testimonial', 'bizskillsacademy-testimonial' ),
			'add_new_item'       => __( 'Add New Biz Testimonial', 'bizskillsacademy-testimonial' ),
			'new_item'           => __( 'New Biz Testimonial', 'bizskillsacademy-testimonial' ),
			'edit_item'          => __( 'Edit Biz Testimonial', 'bizskillsacademy-testimonial' ),
			'view_item'          => __( 'View Biz Testimonial', 'bizskillsacademy-testimonial' ),
			'all_items'          => __( 'All Biz Testimonial', 'bizskillsacademy-testimonial' ),
			'search_items'       => __( 'Search Biz Testimonial', 'bizskillsacademy-testimonial' ),
			'parent_item_colon'  => __( 'Parent Biz Testimonial:', 'bizskillsacademy-testimonial' ),
			'not_found'          => __( 'No books found.', 'bizskillsacademy-testimonial' ),
			'not_found_in_trash' => __( 'No books found in Trash.', 'bizskillsacademy-testimonial' )
		);

		$args = array (
			'labels'             => $labels,
			'description'        => __( 'Description.', 'bizskillsacademy-testimonial' ),
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => false,
			'rewrite'            => array( 'slug' => 'biz_testimonial' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', ),
		);
		
	    register_post_type( 'biz_testimonial', $args );	   		   	
	}	

	function add_sub_setting_menu() {
	    add_submenu_page( 'edit.php?post_type=biz_testimonial', 
	   		__('Info', 'bizskillsacademy-testimonial'), 
	   		__('Info', 'bizskillsacademy-testimonial'), 
	   		'manage_options', 
	   		'biz_setting', 
	   		array( $this, 'biz_sitting_func' ) );
	}

	function biz_sitting_func() {
?>
	  	<div class="wrap">
		<h1>General Information</h1>
		<h3>Displaying Testimonials</h3>
		<p></p>
		<p>You can display testimonials in the content of a page using shortcodes or you can use widgets to display testimonials.</p>
				
		<h4>Using a Shortcode</h4>
		
		<p>To display testimonials create a new page and enter the following shortcode : <br>
			
		<code>[biz_testimonial backcolor='#eee' order='ASC' textlimit='200' btcount='default']</code></p>
		<p></p>
		
		<p><strong>Tips</strong></p>
		<ul>
			<li>* <code>backcolor</code> is the background of biz testimonial. (default=#eee)</li>
			<li>* <code>order</code> is the order by date of biz testimonials. (default=ASC)</li>
			<li>* <code>textlimit</code> is the text limit of biz testimonials to be display. 
			(default=200) </li>
			<li>* <code>btcount</code> is the number of biz testimonials to be display.</li>
		</ul> 
		<div class="clear"></div>
		</div>
<?php
	}

	// Add the Events Meta Boxes
	function add_events_metaboxes() {
		add_meta_box (
			'bt_author', 
			'Author Info', 
			array($this, 'bt_author_meta'), 
			'biz_testimonial', 
			'advanced', 
			'default'
		);	
	}

	// The Author Metabox
	public function bt_author_meta() {
		global $post;
		
		// Noncename needed to verify where the data originated
		echo '<input type="hidden" name="bt_author_noncename" id="bt_author_noncename" 
		value="'.wp_create_nonce( plugin_basename(__FILE__) ).'" />';
		
		// Get the location data if its already been entered
		$bt_author_name    = get_post_meta( $post->ID, '_bt_author_name', true );
		$bt_author_website = get_post_meta( $post->ID, '_bt_author_website', true );
		$bt_author_country = get_post_meta( $post->ID, '_bt_author_country', true );
		$hdn_country_name  = get_post_meta( $post->ID, '_bt_country_name', true );
		if(empty($hdn_country_name)){
			$hdn_country_name = 'Afghanistan';
		}
		
		// Echo out the field
		echo '<label for="_bt_author_name"> Author Name';
		echo '<input type="text" id="_bt_author_name" name="_bt_author_name" value="'.$bt_author_name.'" class="widefat" autocomplete="off" />';
		echo "</level>";
		echo "<br><br>";

		// Echo out the field
		echo '<label for="_bt_author_website"> Website';
		echo '<input type="text" id="_bt_author_website" name="_bt_author_website" 
		value="'.$bt_author_website.'" class="widefat" autocomplete="off" />';
		echo "</level>";
		echo "<br><br>";

		// Echo out the field
		echo '<label for="_bt_author_country"> Country';
		echo "<br>";
		echo '<select id="_bt_author_country" name="_bt_author_country">
			<option value="AF">Afghanistan</option>
			<option value="AX">Åland Islands</option>
			<option value="AL">Albania</option>
			<option value="DZ">Algeria</option>
			<option value="AS">American Samoa</option>
			<option value="AD">Andorra</option>
			<option value="AO">Angola</option>
			<option value="AI">Anguilla</option>
			<option value="AQ">Antarctica</option>
			<option value="AG">Antigua and Barbuda</option>
			<option value="AR">Argentina</option>
			<option value="AM">Armenia</option>
			<option value="AW">Aruba</option>
			<option value="AU">Australia</option>
			<option value="AT">Austria</option>
			<option value="AZ">Azerbaijan</option>
			<option value="BS">Bahamas</option>
			<option value="BH">Bahrain</option>
			<option value="BD">Bangladesh</option>
			<option value="BB">Barbados</option>
			<option value="BY">Belarus</option>
			<option value="BE">Belgium</option>
			<option value="BZ">Belize</option>
			<option value="BJ">Benin</option>
			<option value="BM">Bermuda</option>
			<option value="BT">Bhutan</option>
			<option value="BO">Bolivia, Plurinational State of</option>
			<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
			<option value="BA">Bosnia and Herzegovina</option>
			<option value="BW">Botswana</option>
			<option value="BV">Bouvet Island</option>
			<option value="BR">Brazil</option>
			<option value="IO">British Indian Ocean Territory</option>
			<option value="BN">Brunei Darussalam</option>
			<option value="BG">Bulgaria</option>
			<option value="BF">Burkina Faso</option>
			<option value="BI">Burundi</option>
			<option value="KH">Cambodia</option>
			<option value="CM">Cameroon</option>
			<option value="CA">Canada</option>
			<option value="CV">Cape Verde</option>
			<option value="KY">Cayman Islands</option>
			<option value="CF">Central African Republic</option>
			<option value="TD">Chad</option>
			<option value="CL">Chile</option>
			<option value="CN">China</option>
			<option value="CX">Christmas Island</option>
			<option value="CC">Cocos (Keeling) Islands</option>
			<option value="CO">Colombia</option>
			<option value="KM">Comoros</option>
			<option value="CG">Congo</option>
			<option value="CD">Congo, the Democratic Republic of the</option>
			<option value="CK">Cook Islands</option>
			<option value="CR">Costa Rica</option>
			<option value="CI">Côte d\'Ivoire</option>
			<option value="HR">Croatia</option>
			<option value="CU">Cuba</option>
			<option value="CW">Curaçao</option>
			<option value="CY">Cyprus</option>
			<option value="CZ">Czech Republic</option>
			<option value="DK">Denmark</option>
			<option value="DJ">Djibouti</option>
			<option value="DM">Dominica</option>
			<option value="DO">Dominican Republic</option>
			<option value="EC">Ecuador</option>
			<option value="EG">Egypt</option>
			<option value="SV">El Salvador</option>
			<option value="GQ">Equatorial Guinea</option>
			<option value="ER">Eritrea</option>
			<option value="EE">Estonia</option>
			<option value="ET">Ethiopia</option>
			<option value="FK">Falkland Islands (Malvinas)</option>
			<option value="FO">Faroe Islands</option>
			<option value="FJ">Fiji</option>
			<option value="FI">Finland</option>
			<option value="FR">France</option>
			<option value="GF">French Guiana</option>
			<option value="PF">French Polynesia</option>
			<option value="TF">French Southern Territories</option>
			<option value="GA">Gabon</option>
			<option value="GM">Gambia</option>
			<option value="GE">Georgia</option>
			<option value="DE">Germany</option>
			<option value="GH">Ghana</option>
			<option value="GI">Gibraltar</option>
			<option value="GR">Greece</option>
			<option value="GL">Greenland</option>
			<option value="GD">Grenada</option>
			<option value="GP">Guadeloupe</option>
			<option value="GU">Guam</option>
			<option value="GT">Guatemala</option>
			<option value="GG">Guernsey</option>
			<option value="GN">Guinea</option>
			<option value="GW">Guinea-Bissau</option>
			<option value="GY">Guyana</option>
			<option value="HT">Haiti</option>
			<option value="HM">Heard Island and McDonald Islands</option>
			<option value="VA">Holy See (Vatican City State)</option>
			<option value="HN">Honduras</option>
			<option value="HK">Hong Kong</option>
			<option value="HU">Hungary</option>
			<option value="IS">Iceland</option>
			<option value="IN">India</option>
			<option value="ID">Indonesia</option>
			<option value="IR">Iran, Islamic Republic of</option>
			<option value="IQ">Iraq</option>
			<option value="IE">Ireland</option>
			<option value="IM">Isle of Man</option>
			<option value="IL">Israel</option>
			<option value="IT">Italy</option>
			<option value="JM">Jamaica</option>
			<option value="JP">Japan</option>
			<option value="JE">Jersey</option>
			<option value="JO">Jordan</option>
			<option value="KZ">Kazakhstan</option>
			<option value="KE">Kenya</option>
			<option value="KI">Kiribati</option>
			<option value="KP">Korea, Democratic People\'s Republic of</option>
			<option value="KR">Korea, Republic of</option>
			<option value="KW">Kuwait</option>
			<option value="KG">Kyrgyzstan</option>
			<option value="LA">Lao People\'s Democratic Republic</option>
			<option value="LV">Latvia</option>
			<option value="LB">Lebanon</option>
			<option value="LS">Lesotho</option>
			<option value="LR">Liberia</option>
			<option value="LY">Libya</option>
			<option value="LI">Liechtenstein</option>
			<option value="LT">Lithuania</option>
			<option value="LU">Luxembourg</option>
			<option value="MO">Macao</option>
			<option value="MK">Macedonia, the former Yugoslav Republic of</option>
			<option value="MG">Madagascar</option>
			<option value="MW">Malawi</option>
			<option value="MY">Malaysia</option>
			<option value="MV">Maldives</option>
			<option value="ML">Mali</option>
			<option value="MT">Malta</option>
			<option value="MH">Marshall Islands</option>
			<option value="MQ">Martinique</option>
			<option value="MR">Mauritania</option>
			<option value="MU">Mauritius</option>
			<option value="YT">Mayotte</option>
			<option value="MX">Mexico</option>
			<option value="FM">Micronesia, Federated States of</option>
			<option value="MD">Moldova, Republic of</option>
			<option value="MC">Monaco</option>
			<option value="MN">Mongolia</option>
			<option value="ME">Montenegro</option>
			<option value="MS">Montserrat</option>
			<option value="MA">Morocco</option>
			<option value="MZ">Mozambique</option>
			<option value="MM">Myanmar</option>
			<option value="NA">Namibia</option>
			<option value="NR">Nauru</option>
			<option value="NP">Nepal</option>
			<option value="NL">Netherlands</option>
			<option value="NC">New Caledonia</option>
			<option value="NZ">New Zealand</option>
			<option value="NI">Nicaragua</option>
			<option value="NE">Niger</option>
			<option value="NG">Nigeria</option>
			<option value="NU">Niue</option>
			<option value="NF">Norfolk Island</option>
			<option value="MP">Northern Mariana Islands</option>
			<option value="NO">Norway</option>
			<option value="OM">Oman</option>
			<option value="PK">Pakistan</option>
			<option value="PW">Palau</option>
			<option value="PS">Palestinian Territory, Occupied</option>
			<option value="PA">Panama</option>
			<option value="PG">Papua New Guinea</option>
			<option value="PY">Paraguay</option>
			<option value="PE">Peru</option>
			<option value="PH">Philippines</option>
			<option value="PN">Pitcairn</option>
			<option value="PL">Poland</option>
			<option value="PT">Portugal</option>
			<option value="PR">Puerto Rico</option>
			<option value="QA">Qatar</option>
			<option value="RE">Réunion</option>
			<option value="RO">Romania</option>
			<option value="RU">Russian Federation</option>
			<option value="RW">Rwanda</option>
			<option value="BL">Saint Barthélemy</option>
			<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
			<option value="KN">Saint Kitts and Nevis</option>
			<option value="LC">Saint Lucia</option>
			<option value="MF">Saint Martin (French part)</option>
			<option value="PM">Saint Pierre and Miquelon</option>
			<option value="VC">Saint Vincent and the Grenadines</option>
			<option value="WS">Samoa</option>
			<option value="SM">San Marino</option>
			<option value="ST">Sao Tome and Principe</option>
			<option value="SA">Saudi Arabia</option>
			<option value="SN">Senegal</option>
			<option value="RS">Serbia</option>
			<option value="SC">Seychelles</option>
			<option value="SL">Sierra Leone</option>
			<option value="SG">Singapore</option>
			<option value="SX">Sint Maarten (Dutch part)</option>
			<option value="SK">Slovakia</option>
			<option value="SI">Slovenia</option>
			<option value="SB">Solomon Islands</option>
			<option value="SO">Somalia</option>
			<option value="ZA">South Africa</option>
			<option value="GS">South Georgia and the South Sandwich Islands</option>
			<option value="SS">South Sudan</option>
			<option value="ES">Spain</option>
			<option value="LK">Sri Lanka</option>
			<option value="SD">Sudan</option>
			<option value="SR">Suriname</option>
			<option value="SJ">Svalbard and Jan Mayen</option>
			<option value="SZ">Swaziland</option>
			<option value="SE">Sweden</option>
			<option value="CH">Switzerland</option>
			<option value="SY">Syrian Arab Republic</option>
			<option value="TW">Taiwan, Province of China</option>
			<option value="TJ">Tajikistan</option>
			<option value="TZ">Tanzania, United Republic of</option>
			<option value="TH">Thailand</option>
			<option value="TL">Timor-Leste</option>
			<option value="TG">Togo</option>
			<option value="TK">Tokelau</option>
			<option value="TO">Tonga</option>
			<option value="TT">Trinidad and Tobago</option>
			<option value="TN">Tunisia</option>
			<option value="TR">Turkey</option>
			<option value="TM">Turkmenistan</option>
			<option value="TC">Turks and Caicos Islands</option>
			<option value="TV">Tuvalu</option>
			<option value="UG">Uganda</option>
			<option value="UA">Ukraine</option>
			<option value="AE">United Arab Emirates</option>
			<option value="GB">United Kingdom</option>
			<option value="US">United States</option>
			<option value="UM">United States Minor Outlying Islands</option>
			<option value="UY">Uruguay</option>
			<option value="UZ">Uzbekistan</option>
			<option value="VU">Vanuatu</option>
			<option value="VE">Venezuela, Bolivarian Republic of</option>
			<option value="VN">Viet Nam</option>
			<option value="VG">Virgin Islands, British</option>
			<option value="VI">Virgin Islands, U.S.</option>
			<option value="WF">Wallis and Futuna</option>
			<option value="EH">Western Sahara</option>
			<option value="YE">Yemen</option>
			<option value="ZM">Zambia</option>
			<option value="ZW">Zimbabwe</option>
		</select>';	
		echo '<input type="hidden" name="hdn_country_name" id="hdn_country_name" 
		value="'.$hdn_country_name.'" />';
		echo '</level>';

        echo '<script type="text/javascript">
        (function( $ ) {

      		$(\'#_bt_author_country option[value="'.$bt_author_country.'"]\').attr( 
      		"selected", "selected" );

      		$( "#_bt_author_country" ).change(function() {
				$( "#flag" ).removeAttr(\'class\');
				$( "#flag" ).addClass( "flag flag-"+ $(this).val().toLowerCase() );
				$( "#hdn_country_name" ).val($("#_bt_author_country option:selected").text());
			});

			$( "#_bt_author_website" ).blur(function() {
				var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
				if($(this).val()!="") {
					if (!re.test($(this).val())) { 
					    $(this).val(\' \');
					    return false;
					}
				}
			});

      	})( jQuery );
        </script>';

		echo '<div ><span id="flag" class="flag flag-'.strtolower($bt_author_country).'" alt="Czech Republic"></span></div>';
	}

	// Save the Metabox Data
	function wpt_save_bt_author($post_id, $post) {
		global $post;

		if(!isset($_POST['bt_author_noncename'])){
			return false;
		}
		// verify this came from the our screen and with proper authorization,
		// because save_post can be triggered at other times
		if ( !wp_verify_nonce( $_POST['bt_author_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		}

		// Is the user allowed to edit the post or page?
		if ( !current_user_can( 'edit_post', $post->ID ))
			return $post->ID;

		// OK, we're authenticated: we need to find and save the data
		// We'll put it into an array to make it easier to loop though.
		//_bt_author_name
		//_bt_author_website
		//_bt_author_country		
		//if( $post->post_type == 'revision' ) return; 
		// Add values of $events_meta as custom fields

		if(get_post_meta($post->ID, '_bt_author_name', FALSE)) { 		
			update_post_meta($post->ID, '_bt_author_name', $_POST['_bt_author_name']);
		} else { 			
			add_post_meta($post->ID, '_bt_author_name', $_POST['_bt_author_name']);
		}

		if (!filter_var($_POST['_bt_author_website'], FILTER_VALIDATE_URL) === false) {   	
			if(get_post_meta($post->ID, '_bt_author_website', FALSE)) { 		
				update_post_meta($post->ID, '_bt_author_website', $_POST['_bt_author_website']);
			} else { 			
				add_post_meta($post->ID, '_bt_author_website', $_POST['_bt_author_website']);
			}
		}

		if(get_post_meta($post->ID, '_bt_author_country', FALSE)) { 		
			update_post_meta($post->ID, '_bt_author_country', $_POST['_bt_author_country']);
		} else { 			
			add_post_meta($post->ID, '_bt_author_country', $_POST['_bt_author_country']);
		}

		if(get_post_meta($post->ID, '_bt_country_name', FALSE)) { 		
			update_post_meta($post->ID, '_bt_country_name', $_POST['hdn_country_name']);
		} else { 			
			add_post_meta($post->ID, '_bt_country_name', $_POST['hdn_country_name']);
		}		
	}

}