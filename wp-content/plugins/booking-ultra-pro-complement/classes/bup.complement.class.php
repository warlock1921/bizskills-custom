<?php
class BookingUltraProComplement
{
	public $classes_array = array();
	
	
		
	public function __construct()
	{
		
		global $bookingultrapro;
		$this->load_core();	
				
	
    }
	
	public function load_core() 
	{	
		
		/*Load Amin Classes*/		
		if (is_admin()) 
		{
			$this->set_admin_classes();
			$this->load_classes();					
		
		}else{
			
			/*Load Main classes*/
			$this->set_main_classes();
			$this->load_classes();
			
		
		}
		
				
			
	}
	
	public function plugin_init() 
	{
		global $bookingultrapro;	
		
		if(isset($bookingultrapro))
		{		
			
		
		}
		
		add_action('wp_enqueue_scripts', array(&$this, 'add_scripts'), 15);
		
			
	}
	
 
	public function set_main_classes()
	{
		 $this->classes_array = array("payment" =>"bup.complement.payment.class",
		 
		 "note" =>"bup.complement.note.class",
		  "newsletter" =>"bup.complement.newslettertool",
		  "stripe" =>"bup.complement.stripe.class",
		   "aweber" =>"bup.complement.aweber.class",
		    "location" =>"bup.complement.location.class",
			 "dayoff" =>"bup.complement.dayoff.class",
			  "googlecalendar" =>"bup.complement.googlecalendar.class",
			   "authorize" =>"bup.complement.authorize.class",
			    "profile" =>"bup.complement.profile.class"); 	
	
	}
	
	public function set_admin_classes()
	{
				 
		 $this->classes_array = array("payment" =>"bup.complement.payment.class",
		 
		 "note" =>"bup.complement.note.class",
		 "newsletter" =>"bup.complement.newslettertool",
		 "stripe" =>"bup.complement.stripe.class",
		  "aweber" =>"bup.complement.aweber.class",
		  "location" =>"bup.complement.location.class",
		  "dayoff" =>"bup.complement.dayoff.class",
		   "googlecalendar" =>"bup.complement.googlecalendar.class",
		   "authorize" =>"bup.complement.authorize.class",
		   "profile" =>"bup.complement.profile.class"); 	
		 
		
	}
	
	public function add_scripts()
	{
		global  $bookingultrapro;
		
		//echo "add scripts";
		
		if($bookingultrapro->get_option('gateway_stripe_active')=='1')	
		{
						
			/*Users JS*/		
			wp_register_script( 'bupcopm_stipe_js', 'https://js.stripe.com/v2/',array('jquery'),  null);
			wp_enqueue_script('bupcopm_stipe_js');
			
			wp_register_script( 'bup_stripe_js', bookingup_comp_url.'js/bup-stripe.js', array( 
			'jquery') );
			wp_enqueue_script( 'bup_stripe_js' );
			
			$keys = $this->stripe->get_stripe_keys();
			
			wp_localize_script( 'bup_stripe_js', 'bup_stripe_vars', array(
            'publishable_key'     => $keys['publishable_key']
            
            
        		) ); 	
		
		}
		
		if($bookingultrapro->get_option('gateway_authorize_active')=='1')	
		{			
			wp_register_script( 'bup_authorize_js', bookingup_comp_url.'js/bup-authorize.js', array( 
			'jquery') );
			wp_enqueue_script( 'bup_authorize_js' );
			
		}
		 
		
	}
	
	public function get_all_locations_front_booking ()
	{
		global $wpdb;
				
		$sql = ' SELECT * FROM ' . $wpdb->prefix . 'bup_filters ORDER BY filter_name ' ;								
		$rows = $wpdb->get_results($sql);
			
		$selected ='';

		
		$count = 0;
		
		$html = '';		
		$html .= '<select name="bup-filter-id" id="bup-filter-id">';
		$html .= '<option value="" selected="selected" >'.__('Select Location', 'bookingup').'</option>';
				
		if (!empty($rows))
		{
			
			foreach($rows as $filter) 
			{
				
		
				$html .= '<option value="'.$filter->filter_id.'" '.$selected.'>'.$filter->filter_name.'</option>';
				
				
			}
			$html .= '</select>';
					
		
		}
		
		return $html;
		
				
	}
	
	
	
	
	
	
	function load_classes() 
	{	
		
		foreach ($this->classes_array as $key => $class) 
		{
			if (file_exists(bookingup_comp_path."classes/$class.php")) 
			{
				require_once(bookingup_comp_path."classes/$class.php");
						
					
			}
				
		}	
	}
	

}
?>